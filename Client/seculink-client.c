#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <errno.h>
#include <math.h>
//#include <netinet/tcp_seq.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>

#include <linux/types.h>
#include <errno.h>
#include <sys/time.h>
#include <pthread.h>
#include <signal.h>


#include <netinet/tcp.h>
#include <netinet/udp.h>
#include <arpa/inet.h>

#define QSIZE 1000
#define PSIZE_MAX 2048
#define PRECISION 100000
#define MAX_PCOUNT 1
#define MAX(A,B) (((A) >= (B)) ? (A) : (B))
#define MIN(A,B) (((A) < (B)) ? (A) : (B))


// packet types
#define PKT_TCP 'E'
#define PKT_DUMMY 'D'
#define PKT_SYNC 'S'
#define PKT_HELLO 'H'
#define PKT_BEGIN 'B'
#define PKT_END 'F'

extern int errno;

//Socket variables
int sockfd, n;
struct sockaddr_in servaddr;
char msg_hello[16] = "";
char msg_dummy[13] = "DUMMY PACKET\n";
char msg_begin[6] = "BEGIN\n";
char msg_end[7] = "FINISH\n";
char msg_ack[4] = "ACK\n";
char buf[PSIZE_MAX] __attribute__ ((aligned));
int len;
char in_line[1000];

// struct itimerspec value;

// Netfilter queue variables
int rv;
FILE *fp = 0;
FILE *fp2 = 0;
short test_num = 0;
char filename[100] = "packets.txt";
char path[200] = "/";

// Hazard avoidance variables
// int syns = 0;
// int acks = 0;
pthread_mutex_t lock;
short q_flg = 1;
char send_buf[PSIZE_MAX] __attribute__ ((aligned));
int send_size;
short first_send = 1;
short begin = 0;
short debug = 0;
short noslot = 0;
short store = 0;
short adaptive = 0;// Time slotting variables
short adaptive_type = 0;
int pkt_size_q[QSIZE];
struct timeval pkt_tstamp_q[QSIZE];
char pkt_q[QSIZE][PSIZE_MAX];
int head_idx = 0;
int tail_idx = 0;
int q_len = 0;
double q = 0.0; // default q prob
double p = 1.0;
double beta = 0.9;
short m_value = 100;
int n_value = 1024;
int q_param = 0;
int p_param = 0;
int power_value = 1;
// int slot = 1000; // 1 millisecond: default slot time
int heartbeat = 1000;
int slack = 0;
int c_slot = 1;
int c_len = 1;
int c_rate = 2147483647;
int t_len = 1;
int tick = 0;
int stick = 0;

short fetch = 0;
int packet_count = 0;
int arrival_count = 0;
double service_rate = 0.0;
int old_count = 0;
int dummy_count = 0;
int packet_begin = 0;
int packet_rate;
int toggle_rate = 2147483647;
struct timeval fetch_begin;
struct timeval sample_begin;

double calculate_service(double srate, double qlen)
{
	double result = srate;
	return result;
}

void print_packet(unsigned char *packet_data, int size)
{
	struct iphdr *ip;
	struct tcphdr *tcp;
	struct udphdr *udp;	
	struct in_addr ipa;
	char src_ip[20];
	char dst_ip[20];
	unsigned short src_port;
	unsigned short dst_port;

	ip = (struct iphdr *) packet_data;
	ipa.s_addr=ip->saddr;
	strcpy (src_ip, inet_ntoa(ipa));
	ipa.s_addr=ip->daddr;
	strcpy (dst_ip, inet_ntoa(ipa));

	if(ip->protocol == IPPROTO_UDP) {
		udp = (struct udphdr *)(packet_data + sizeof(*ip));
		src_port = ntohs(udp->source);
		dst_port = ntohs(udp->dest);
		fprintf(stdout, "%s(%d) --> %s(%d) - p_size: %d\n", src_ip, src_port, dst_ip, dst_port, size);
	} else 	if(ip->protocol == IPPROTO_TCP) {
		tcp = (struct tcphdr *)(packet_data + sizeof(*ip) + sizeof(*udp) + sizeof(*ip));
		src_port = ntohs(tcp->source);
		dst_port = ntohs(tcp->dest);
		fprintf(stdout, "%s(%d) --> %s(%d) - p_size: %d\n", src_ip, src_port, dst_ip, dst_port, size);
	}
	
	return;
}

void* read_input()
{
	struct timeval current;
	char num[100] = "";
	int i = 0;

	while(fgets(filename, 100, stdin) != NULL)
	{
		// printf("%s",filename);
		if(filename[0] == 'T' && filename[1] == 'E' && filename[2] == 'R')
		{
			if(store)
			{
				store = 0;
				fclose(fp);
				fclose(fp2);
			}
			exit(0);
		}
		else if(filename[0] == 'C' && filename[1] == 'L' && filename[2] == 'O')
		{
			if(store)
			{
				store = 0;
				fclose(fp);
				fclose(fp2);
			}
			gettimeofday(&current,NULL);
			double elapsed = (current.tv_sec - sample_begin.tv_sec) + (double)(current.tv_usec - sample_begin.tv_usec) / 1000000;
			printf("Packet count: %d = %d (real) + %d (dummy)\n",(packet_count + dummy_count),packet_count,dummy_count);
			printf("Link speed: %f KBps, lapsed: %f\n", (double)(dummy_count + packet_count) * 1.46 / elapsed,elapsed);
		}
		else if(filename[0] == 'B' && filename[1] == 'E' && filename[2] == 'G')
		{
			if(fetch) // fetch end didn't receive
			{
				// shouldn't happen
			}
			gettimeofday(&current,NULL);
			fetch_begin = current;
			packet_begin = packet_count;
			fetch = 1;
			if(store)
			{
				// pthread_mutex_lock(&lock);
				fprintf(fp,"B,%lu.%06lu,NaN,NaN,NaN\n",
								current.tv_sec,
								current.tv_usec);
				// pthread_mutex_unlock(&lock);
			}
			printf("MSG: BEGAN %lu.%06lu\n",current.tv_sec,current.tv_usec);
		}
		else if(filename[0] == 'E' && filename[1] == 'N' && filename[2] == 'D')
		{
			if(fetch) // ignore duplicate end
			{// double fetch end
				gettimeofday(&current,NULL);
				fetch = 0;
				if(store)
				{
					// pthread_mutex_lock(&lock);
					fprintf(fp,"E,%lu.%06lu,%06d,%lu.%06lu,%f\n",
									current.tv_sec,
									current.tv_usec,
									(packet_count - packet_begin),
									fetch_begin.tv_sec, 
									fetch_begin.tv_usec,
									((double)(current.tv_sec - fetch_begin.tv_sec) + 
									(double)(current.tv_usec - fetch_begin.tv_usec) / 1000000));
					// pthread_mutex_unlock(&lock);
				}
				printf("MSG: ENDED %lu.%06lu\n",current.tv_sec,current.tv_usec);
			}
		}
		else if (filename[0] == 'C' && filename[1] == 'O' && filename[2] == 'M')
		{
			for (i = 7 ; i < 100 ; i++)
				num[i - 7] = filename[i];

			pthread_mutex_lock(&lock);
			/* if(filename[4] == 'G' && filename[5] == 'L')
			{
				hi_len = hi_len / (red + green);
				lo_len = lo_len / (red + green);
				green = atoi(num);
				hi_len = hi_len * (red + green);
				lo_len = lo_len * (red + green);
			}
			else if(filename[4] == 'R' && filename[5] == 'L')
			{
				hi_len = hi_len / (red + green);
				lo_len = lo_len / (red + green);
				red = atoi(num);
				hi_len = hi_len * (red + green);
				lo_len = lo_len * (red + green);
			} */
			if(filename[4] == 'C' && filename[5] == 'R') { c_rate = atoi(num); } // printf("hi_rate = %d\n",hi_rate);}
			else if(filename[4] == 'C' && filename[5] == 'L') { c_len = atoi(num); }
			else if(filename[4] == 'T' && filename[5] == 'L') { t_len = atoi(num); }
			else if(filename[4] == 'C' && filename[5] == 'S') { c_slot = atoi(num); }
			else if(filename[4] == 'Q' && filename[5] == 'R') { q = atof(num); }
			else if(filename[4] == 'T' && filename[5] == 'R') { toggle_rate = atoi(num); }

			pthread_mutex_unlock(&lock);
		}
		else
		{
			if(store)
			{
				store = 0;
				fclose(fp);
				fclose(fp2);
			}
			char *pos;
			if ((pos=strchr(filename, '\n')) != NULL)
				*pos = '\0';			sprintf(path,"logs/%s-up.log",filename);  
			fp = fopen(path,"w");
			sprintf(path,"logs/%s-down.log",filename);  
			fp2 = fopen(path,"w");
			packet_count = 0;
			dummy_count = 0;
			old_count = 0;
			gettimeofday(&current,NULL);
			sample_begin = current;
			store = 1;
		}
	}
	return NULL;
}

void send_ack()
{
	sendto(sockfd, msg_ack, 4, 0,
		(struct sockaddr *)&servaddr, sizeof(servaddr));
	return;
}
// thread for reading packets from udp tunnel
void* handle_packet()
{
	struct timeval current;
	for(;;)
	{
		if ((rv = recv(sockfd, buf, sizeof(buf), 0)) >= 0) {
			if (buf[0] == 'S')
			{
//				printf("%lu.%06lu SYNC RECEIVED.\n",current.tv_sec,current.tv_usec);
//				printf("sync received %d,%d.\n",syns++,acks);
				send_ack();
				gettimeofday(&current,NULL);
//				printf("%lu.%06lu ACK SENT.\n",current.tv_sec,current.tv_usec);
			}
			continue;
		}
		if (rv < 0 && errno == ENOBUFS) {
			printf("losing packets!\n");
			continue;
		}
		perror("recv failed");
		break;
	}
	return NULL;
}


int main (int argc, char** argv)	
{
	int i = 0;
	int hrset = 0;
	//int lrset = 0;
	//int lsset = 0;

	// int hsset = 0;
	// int lsset = 0;
	for (i = 1; i < argc ; i++)
	{
		
		// printf("%d %s\n",argc, argv[i]);
		if(!strcmp(argv[i],"-s"))
		{
			c_slot = atoi(argv[++i]);
		}
		else if(!strcmp(argv[i],"-p"))
		{
			p = atof(argv[++i]);
		}
		else if(!strcmp(argv[i],"-q"))
		{
			q = atof(argv[++i]);
			begin = 1;
		}
		else if(!strcmp(argv[i],"-l"))
			c_len = atoi(argv[++i]);
		else if(!strcmp(argv[i],"-t"))
			t_len = atoi(argv[++i]);
		else if(!strcmp(argv[i],"-r"))
		{
			c_rate = atoi(argv[++i]);
			hrset = 1;
		}
		else if(!strcmp(argv[i],"-tr"))
			toggle_rate = atoi(argv[++i]);
		else if(!strcmp(argv[i],"-b"))
			beta = atof(argv[++i]);
		else if(!strcmp(argv[i],"-m")) // alpha
			m_value = atoi(argv[++i]);
		else if(!strcmp(argv[i],"-n")) // rate
			n_value = atoi(argv[++i]);
		else if(!strcmp(argv[i],"-test")) // rate
			test_num = atof(argv[++i]);
		else if(!strcmp(argv[i],"-adaptive"))
		{
			adaptive = 1;
			adaptive_type = atoi(argv[++i]) - 1;
			if (adaptive_type == 1 || adaptive_type == 3) // doubling
				p = 0.0;
		}
		else if(!strcmp(argv[i],"-debug"))
			debug = 1;
		else if(!strcmp(argv[i],"-noslot"))
			noslot = 1;
		else if(!strcmp(argv[i],"-store"))
		{
			sprintf(filename,"%s",argv[++i]);
			store = 1;
		}
		else
		{
			printf("Usage: udpclient [OPTION]\n");
			printf("  -s,\t\t slot size in milliseconds - default: 1\n");
			printf("  -q,\t\t probability of sending dummy packets - default: 0\n");
			printf("  -l,\t\t length of cycle as multiplier of small cycle - default: small cycle\n");
			printf("  -r,\t\t packets per slot - default: max\n");
			printf("  -t,\t\t total cycle length as multiplier of small cycle - default: small cycle\n");
			printf("  -tr,\t\t packet rate to toggle (in adaptive cycle change) - default: Inf\n");
			printf("  -m,\t\t pass an integer argument to the server - default: 100\n");
			printf("  -n,\t\t pass a short argument to the server - default: 1024\n");
			printf("  -noslot,\t no time slotting\n");
			printf("  -adaptive,\t adaptive cycle change\n");
			printf("  -debug,\t run in debug mode\n");
			printf("  -store,\t store timestamps\n");
			return 1;
		}
	}

	if(noslot)
		begin = 0;
	if(!hrset)
	{
		if (begin)
		{
			printf("cycle rate is set to 10 packets by default\n");
			c_rate = 10;
		}
		else
			c_rate = c_slot * 1000 / 50;
	}
	int bad_input = 0;
	/* if((lrset || lsset) && (!adaptive) && (lo_len == 0))
	{
		printf("Input Error: parameters of low cycle set but cycle length is zero.\n");
		printf("cycle rate: %d, cycle slot size: %d, cycle length: %d\n",lo_rate,lo_slot,lo_len);
		bad_input = 1;
	} */
	if(c_slot * 1000 / 50 < c_rate)
	{
		printf("Input Error: high cycle rate higher than capacity.\n");
		printf("cycle rate: %d, max rate: %d\n",c_rate,(c_slot * 1000 / 50));
		bad_input = 1;
	}
	if(bad_input)
	{
		printf("For instructions type: udpclient -?\n");
		return 1;
	}
	// printf("green = %d, red = %d, q = %f\n",green,red,q);
	printf("slot = %d(ms) , len = %d, rate = %d\n", c_slot , c_len, c_rate);
	// printf("low slot = %d(ms), low len = %d, low rate = %d \n", lo_slot , lo_len, lo_rate);
	if(store)
	{
		char *pos;
		if ((pos=strchr(filename, '\n')) != NULL)
			*pos = '\0';
		sprintf(path,"logs/%s-up.log",filename);  
		fp = fopen(path,"w");
		sprintf(path,"logs/%s-down.log",filename);  
		fp2 = fopen(path,"w");
	}		
	
	sockfd = socket(AF_INET,SOCK_DGRAM,0);
	
	bzero(&servaddr, sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	// change address if server address is set to something different
	servaddr.sin_addr.s_addr = inet_addr("192.168.1.1");
	servaddr.sin_port = htons(47385);
	
	printf("m_value: %d, n_value: %d\n",m_value,n_value);	
	msg_hello[0] = 'H';
	msg_hello[1] = 'E';
	msg_hello[2] = 'L';
	msg_hello[3] = 'L';
	msg_hello[4] = 'O';
	msg_hello[5] = ' ';
	msg_hello[6] = m_value & 0xFF;
	msg_hello[7] = (m_value >> 8 ) & 0xFF;
	msg_hello[8] = ' ';
	msg_hello[9] = n_value & 0xFF;
	msg_hello[10] = (n_value >> 8) & 0xFF;
	msg_hello[11] = (n_value >> 16) & 0xFF;
	msg_hello[12] = (n_value >> 24) & 0xFF;
	msg_hello[13] = ' ';
	msg_hello[14] = test_num & 0xFF;
	msg_hello[15] = (test_num >> 8 ) & 0xFF;

	short pp = -1;
	memcpy(&pp,msg_hello + 6,sizeof(pp));
	int rr = -1;
	memcpy(&rr,msg_hello + 9,sizeof(rr));
	short tn = -1;
	memcpy(&tn,msg_hello + 14,sizeof(tn));
	printf("test: %d, sent_m: %d, sent_n: %d\n", tn, pp, rr);
	sendto(sockfd, msg_hello, 16, 0,
			(struct sockaddr *)&servaddr, sizeof(servaddr)); // Initiate UDP channel by sending hello!
	// print("hello sent\n");
	
	// pthread_t heartbeat_thread;
	pthread_t send_thread;
	pthread_t read_thread;

	// struct sigaction sa;
	sigset_t newset;
	sigemptyset (&newset);
	sigaddset(&newset, SIGALRM);
	sigprocmask( SIG_BLOCK, &newset, NULL);
	
	pthread_mutex_init(&lock, NULL);
	
	// pthread_create(&heartbeat_thread, NULL, handle_heartbeat, NULL);
	pthread_create(&send_thread, NULL, handle_packet, NULL);
	pthread_create(&read_thread, NULL, read_input, NULL);
	
	// pthread_join(heartbeat_thread, NULL);
	pthread_join(send_thread, NULL);
	pthread_join(read_thread, NULL);


	pthread_mutex_destroy(&lock);
	
	exit(0);
}
