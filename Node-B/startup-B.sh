#!/bin/bash

# sudo ifconfig eth0 up
# sudo dhclient eth0
sudo route add default gw 10.220.3.2
sudo route del default gw 10.220.3.1
sudo ifconfig eth2 192.200.1.1/24 up
sudo service setkey restart
sudo service racoon restart
./port-tunnel-server.sh
