// current_client : start from zero and merge with client id

#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
//#include <netinet/tcp_seq.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>

#include <linux/types.h>
#include <linux/netfilter.h>		/* for NF_ACCEPT */
#include <errno.h>
#include <libnetfilter_queue/libnetfilter_queue.h>
#include <sys/time.h>
#include <pthread.h>
#include <signal.h>


#include <netinet/tcp.h>
#include <netinet/udp.h>
#include <arpa/inet.h>

#define MAX_ACTIVE_TRACES 500
#define MAX_RATE 20
#define PSIZE_MAX 2048
#define QSIZE 20000
#define FILTER_QSIZE 20000
#define CLIENTS 1
#define PRECISION 100000
// #define MAX_PCOUNT 1
#define MAX(A,B) (((A) >= (B)) ? (A) : (B))
#define MIN(A,B) (((A) < (B)) ? (A) : (B))
#define ABS(A) (((A) > 0) ? (A) : (-A))

// packet types
#define PKT_TCP 'E'
#define PKT_DUMMY 'D'
#define PKT_HELLO 'H'
#define PKT_BEGIN 'B'
#define PKT_END 'F'
#define PKT_ACK 'A'
#define PKT_RATE 'R'
#define PKT_WRAP 'W'

#define __FAVOR_BSD

extern int errno;

//Socket variables
int sockfd, n;
// short is_hello = 1;
short first_time = 0;
struct sockaddr_in servaddr, cliaddr;

socklen_t len;
char msg_hello[6] = "HELLO\n";
char msg_dummy[13] = "DUMMY PACKET\n";
char msg_sync[14] = "";

char buf[PSIZE_MAX] __attribute__ ((aligned));

// Netfilter queue variables
int rv;
struct nfq_handle *h;
struct nfq_q_handle *sqh, *rqh;
struct nfnl_handle *nh;
int fd;
FILE *fp = 0;
FILE *fp2 = 0;
FILE *fp3 = 0;
FILE *fp4 = 0;
FILE *fp5 = 0;
short test_num = 0;
char filename[100] = "packets.txt";
char path[200] = "/";

// Hazard avoidance variables
pthread_mutex_t lock;
short q_flg = 0;
char send_buf[PSIZE_MAX] __attribute__ ((aligned));
int send_size;
short first_send = 1;
int begin = 0;
int debug = 0;
int noslot = 0;
short store = 0;

int store_total_rate;
int store_total_arrival;
int store_total_processed;
double store_total_delay;
int store_total_dummy;
int store_total_loss;

int store_min_active;
int store_total_active;
int store_max_active;

int store_min_x;
int store_total_x;
int store_max_x;

double store_min_y;
double store_total_y;
double store_max_y;

int heartbeat_total_arrival;
int heartbeat_total_rate;
int heartbeat_total_loss;

int store_total_slot;

double store_y_value = -1.0;
double store_alpha = -1.0;
struct timeval store_begin_ts;
short adaptive = 0;
long hb_count = 0;
int total_dummy = 0;
int total_loss = 0;

int total_reused = 0;

double xy_diff = 0;
double qmax = 10000000.0;

struct packet
{
	char pkt[PSIZE_MAX];
	int pkt_size;
	uint16_t pkt_port;
	struct timeval pkt_tstamp;
};

int last_added_id = -1;
int first_added_id = -1;

struct trace
{
	int next_id;
	int prev_id;
	int idx;
	int active;
};

	int update_cycle;
	int slot;
	int rate;
	short super_trace[40000];
	int strace_len;
	int strace_pcount;
	int active_traces;
	int current_sidx[MAX_ACTIVE_TRACES];
	int trace[MAX_ACTIVE_TRACES]; // array of traces: 1 is active 0 means inactive
	int total_rate;
	// optimization variables
	int x_value;
	double y_value;
	double sigma;
	double alpha;
	double beta;
	double arr_beta;
	double gamma_value;
	double epsilon;
	double err;
	int last_err;
	double lambda;
	double mu;
	int min_q_len;
	int avg_q_len;
	int avg_lambda;
	int max_arrival;
	int last_slot_total_arrival;
	double avg_arrival;
	int cycle_rate;
	
struct client
{
	int id;
	struct sockaddr_in addr;
	int next_id;
	int prev_id;

	// packet queue
	struct packet pkts[QSIZE];
	int head_idx;
	int tail_idx;
	int q_len;
	int last_q_len;

//	int slot;
//	int rate;
	short ack_received;
	short first_time;
//	short begin;
	short active;

//	short silent;

//	short super_trace[40000];
//	int current_sidx;
//	int strace_len;

	struct timeval delay;
	int reused;
	int arrival;
	int heartbeat_arrival;
	int loss;
	int last_slot_arrival;
	int processed;
	int heartbeat_processed;
	short flag;

	int missing_acks;

	int cycle_arrival;
};

// Time slotting variables
struct client middle;

double p = 1.0;
double q = 0.0;
int p_param = 0;
int q_param = 0;
int power_value = 1;
int heartbeat = 1000;
int slack = 0;
// int tick = 0;
int stick = 0;

short fetch = 0;
int toggle_rate = 2147483647;
struct timeval fetch_begin;
struct timeval sample_begin;

void send_sync()
{
	msg_sync[0] = 'S';
	msg_sync[1] = 'Y';
	msg_sync[2] = 'N';
	msg_sync[3] = 'C';
	msg_sync[4] = ' ';

	int tmp = stick;		
	msg_sync[5] = (tmp >> 24) & 0xFF;
	msg_sync[6] = (tmp >> 16) & 0xFF;
	msg_sync[7] = (tmp >> 8) & 0xFF;
	msg_sync[8] = tmp & 0xFF;

	sendto(sockfd, msg_sync, 14, 0, (struct sockaddr *)&middle.addr, sizeof(middle.addr));
	return;
}

void show_tcp(unsigned char *packet_data, int size)
{
	struct iphdr *ip;
	struct tcphdr *tcp;
	struct in_addr ipa;
	char src_ip_str[20];
	char dst_ip_str[20];
	unsigned short src_port_str;
	unsigned short dst_port_str;
//	tcp_seq seq, ack;
	
	ip = (struct iphdr *) packet_data;
	ipa.s_addr=ip->saddr;
	strcpy (src_ip_str, inet_ntoa(ipa));
	ipa.s_addr=ip->daddr;
	strcpy (dst_ip_str, inet_ntoa(ipa));

	if(ip->protocol == IPPROTO_TCP) {
		tcp = (struct tcphdr *)(packet_data + sizeof(*ip));
		src_port_str = ntohs(tcp->source);
		dst_port_str = ntohs(tcp->dest); //th_dport);
	}	
	fprintf(stdout, "%s(%d) --> %s(%d) - p_size: %d\n", src_ip_str, src_port_str, dst_ip_str,dst_port_str,size);
	return;
}

void print_headers(unsigned char * pkt)
{
	struct iphdr *uip = (struct iphdr *)pkt;
	struct sockaddr_in tmpaddr;
	tmpaddr.sin_addr.s_addr = uip->saddr;
	printf("pkt %s -> ",inet_ntoa(tmpaddr.sin_addr));
	tmpaddr.sin_addr.s_addr = uip->daddr;
	printf("%s\n",inet_ntoa(tmpaddr.sin_addr));
	return;
}

void show_udp(unsigned char *packet_data, int size)
{
	struct iphdr *ip;
	struct iphdr *uip;
	struct tcphdr *tcp;
	struct udphdr *udp;	
	struct in_addr ipa;
	char u_src_ip[20];
	char u_dst_ip[20];
	unsigned short u_src_port;
	unsigned short u_dst_port;
	char src_ip[20];
	char dst_ip[20];
	unsigned short src_port;
	unsigned short dst_port;
	
	uip = (struct iphdr *) packet_data;
	ipa.s_addr=uip->saddr;
	strcpy (u_src_ip, inet_ntoa(ipa));
	ipa.s_addr=uip->daddr;
	strcpy (u_dst_ip, inet_ntoa(ipa));

	if(uip->protocol == IPPROTO_UDP) {
		udp = (struct udphdr *)(packet_data + sizeof(*uip));
		u_src_port = ntohs(udp->source);
		u_dst_port = ntohs(udp->dest); //th_dport);
	}
	
	ip = (struct iphdr *)(packet_data + sizeof(*uip) + sizeof(*udp));
	ipa.s_addr=ip->saddr;
	strcpy (src_ip, inet_ntoa(ipa));
	ipa.s_addr=ip->daddr;
	strcpy (dst_ip, inet_ntoa(ipa));
	
	if(ip->protocol == IPPROTO_TCP) {
		tcp = (struct tcphdr *)(packet_data + sizeof(*uip) + sizeof(*udp) + sizeof(*ip));
		src_port = ntohs(tcp->source);
		dst_port = ntohs(tcp->dest); //th_dport);
	}

	fprintf(stdout, "%s(%d) --> %s(%d) - p_size: %d\n", u_src_ip, u_src_port, u_dst_ip, u_dst_port,size);
	fprintf(stdout, "\t %s(%d) --> %s(%d)\n", src_ip, src_port, dst_ip, dst_port);
	return;
}

/* void print_packet(unsigned char *packet_data, int size)
{
	struct iphdr *ip;
	struct tcphdr *tcp;
	struct udphdr *udp;	
	struct in_addr ipa;
	char src_ip[20];
	char dst_ip[20];
	uint16_t src_port;
	uint16_t dst_port;
	
	ip = (struct iphdr *) packet_data;
	ipa.s_addr=ip->saddr;
	strcpy (src_ip, inet_ntoa(ipa));
	ipa.s_addr=ip->daddr;
	strcpy (dst_ip, inet_ntoa(ipa));

	if(ip->protocol == IPPROTO_UDP) {
		udp = (struct udphdr *)(packet_data + sizeof(*ip));
		src_port = ntohs(udp->source);
		dst_port = ntohs(udp->dest); //th_dport);
		fprintf(stdout, "UDP: %s(%u) --> %s(%u) - p_size: %d\n", src_ip, src_port, dst_ip, dst_port,size);
	} else if (ip->protocol == IPPROTO_TCP)
	{
		tcp = (struct tcphdr *)(packet_data + sizeof(*ip));
		src_port = ntohs(tcp->th_sport);
		dst_port = ntohs(tcp->th_dport);
		fprintf(stdout, "TCP: %s(%u) --> %s(%u) - p_size: %d\n", src_ip, src_port, dst_ip, dst_port,size);
	}
	return;
} */

int packet_type(unsigned char *data, int size)
{
	unsigned char type = (unsigned char)*data;
	switch(type)
	{
		case 'E': // tcp packet
			return 0;
		case 'D': // dummy packet
			return 1;
		case 'B': // begin
			return 2;
		case 'F': // finish
			return 3;
		default: // nothing
			return -1;
	}
	//show_tcp(data,size);
}

void read_strace(int slot, short* trace, int * tlen, int * pcount)
{

	char path[100];
	pcount[0] = 0;
	char *line = NULL;
	size_t len = 0;
	ssize_t read;
	sprintf(path,"strace-%d.trc",slot);
	FILE * fp = fopen(path,"r");
	tlen[0] = 0;
//	int max_rate = -1;
//	short temp_trace[1000 / slot];
//	int temp_count = 0;
//	int temp_len = 0;
//	printf("sec_split: ");
	while ((read = getline(&line, &len, fp)) != -1)
	{
		trace[tlen[0]] = atoi(line);
		pcount[0] += (double)trace[tlen[0]];
		tlen[0]++;

/*		temp_trace[temp_len] = atoi(line);
		temp_count += temp_trace[temp_len];
		temp_len++;

		if(temp_len == 1000) // begining of a second
		{
			if (temp_count > max_rate)
			{
				int i;
				for(i = 0 ; i < 1000 / slot; i++)
					trace[i] = temp_trace[i];
				pcount[0] = temp_count;
				tlen[0] = 1000 / slot;
				max_rate = temp_count;
			}
			printf("%d, ",temp_count);
			temp_count = 0;
			temp_len = 0;
		} */
	}
	fclose(fp);
	printf("\npcount: %d, len: %d, avg: %f\n",pcount[0],tlen[0],((double)pcount[0] / tlen[0]));
	if (line)
		free(line);
/*	int pad = 2000 / slot;
	int i = 0;
	for (i = 0; i < pad; i++)
	{
		trace[tlen[0]] = 0;
		tlen[0]++;
	} */
	return;
}

void read_strace2(int tmp_len, int tmp_rate, short* trace, int * tlen, int * pcount)
{

	char path[100];
	pcount[0] = 0;
	char *line = NULL;
	size_t len = 0;
	ssize_t read;
	sprintf(path,"traces/strace-%d-%d.trc",tmp_len,tmp_rate);
	FILE * fp = fopen(path,"r");
	tlen[0] = 0;
	while ((read = getline(&line, &len, fp)) != -1)
	{
		trace[tlen[0]] = atoi(line);
		pcount[0] += (double)trace[tlen[0]];
		tlen[0]++;

	}
	fclose(fp);
	printf("\npcount: %d, len: %d, avg: %f\n",pcount[0],tlen[0],((double)pcount[0] / tlen[0]));
	if (line)
		free(line);
	return;
}

void* read_input()
{
	struct timeval current;
	char num[100] = "";
	int i = 0;

	while(fgets(filename, 100, stdin) != NULL)
	{
		// printf("%s",filename);
		if(filename[0] == 'T' && filename[1] == 'E' && filename[2] == 'R')
		{
			if(store)
			{
				store = 0;
				fclose(fp);
				fclose(fp2);
			}
			exit(0);
		}
		else if(filename[0] == 'C' && filename[1] == 'L' && filename[2] == 'O')
		{
			if(store)
			{
				store = 0;
				fclose(fp);
				fclose(fp2);
			}
		}
		else if(filename[0] == 'B' && filename[1] == 'E' && filename[2] == 'G')
		{
			if(fetch) // fetch end didn't receive
			{
				// shouldn't happen
			}
			gettimeofday(&current,NULL);
			fetch_begin = current;
			fetch = 1;
			if(store)
			{
				// pthread_mutex_lock(&lock);
				fprintf(fp,"B,%lu.%06lu,NaN,NaN,NaN\n",
								current.tv_sec,
								current.tv_usec);
				// pthread_mutex_unlock(&lock);
			}
			printf("MSG: BEGAN %lu.%06lu\n",current.tv_sec,current.tv_usec);
		}
		else if(filename[0] == 'E' && filename[1] == 'N' && filename[2] == 'D')
		{
			if(fetch) // ignore duplicate end
			{// double fetch end
				gettimeofday(&current,NULL);
				fetch = 0;
				printf("MSG: ENDED %lu.%06lu\n",current.tv_sec,current.tv_usec);
			}
		}
		else if (filename[0] == 'C' && filename[1] == 'O' && filename[2] == 'M')
		{
			for (i = 8 ; i < 100 ; i++)
				num[i - 8] = filename[i];

			pthread_mutex_lock(&lock);
			if(filename[4] == 'C' && filename[5] == 'R') { rate = atoi(num); } // printf("hi_rate = %d\n",hi_rate);}
			else if(filename[4] == 'C' && filename[5] == 'S') { slot = atoi(num); }
			else if(filename[4] == 'Q' && filename[5] == 'R') { q = atof(num); }
			else if(filename[4] == 'T' && filename[5] == 'R') { toggle_rate = atoi(num); }

			pthread_mutex_unlock(&lock);
		}
		else
		{
			if(store)
			{
				store = 0;
				fclose(fp);
				fclose(fp2);
				fclose(fp3);
			}
			store_total_rate = 0;
			store_total_arrival = 0;
			store_total_processed = 0;
			store_total_delay = 0.0;
			store_total_dummy = 0;
			store_total_loss = 0;
			store_min_active = MAX_ACTIVE_TRACES + 1;
			store_total_active = 0;
			store_max_active = -1;
			store_min_x = MAX_ACTIVE_TRACES + 1;
			store_total_x = 0;
			store_max_x = -1;
			store_min_y = MAX_ACTIVE_TRACES + 1;
			store_total_y = 0;
			store_max_y = -1;
			store_total_slot = 0;
			gettimeofday(&store_begin_ts,NULL);
			char *pos;
			if ((pos=strchr(filename, '\n')) != NULL)
			    *pos = '\0';
			sprintf(path,"logs/%s-up.log",filename);  
			fp = fopen(path,"w");
			sprintf(path,"logs/%s-down.log",filename);  
			fp2 = fopen(path,"w");
			sprintf(path,"logs/%s-err.log",filename);  
			fp3 = fopen(path,"w");
			store = 1;
		}
	}
	return NULL;
}

// packets to be sent to client: instantly or queued to: queue 0
static int send_cb(struct nfq_q_handle *qh, struct nfgenmsg *nfmsg,
			struct nfq_data *nfa, void *data)
{
	int i;
	struct timeval current;
	unsigned char *buf_data = NULL;
	uint16_t src_port;
	// unsigned char buf_data2[PSIZE_MAX];

	if(first_send)
	{
		// printf("we got here.\n");
		memcpy(send_buf, buf, rv);
		send_size = rv;
		first_send = 0;
	}
	/* queue: flag passed to call back to tell whether handle_packet is called
	   from inside the program or outside */
	short queue = *((short *)data);
	u_int32_t id = 0;
	struct nfqnl_msg_packet_hdr *ph = nfq_get_msg_packet_hdr(nfa);
	if (ph) {
		id = ntohl(ph->packet_id);
	}

	if (nfq_get_payload(nfa, &buf_data) == -1)
	{
		printf("send_cb nfq_get_payload error.\n");
		return nfq_set_verdict(qh, id, NF_DROP, 0, NULL);  // how to treat the pkt after queue
	}
	struct iphdr *ip = (struct iphdr *) buf_data;
	struct udphdr *udp;

	gettimeofday(&current, NULL);
	// u_int32_t dst_addr = (unsigned char)(uip->daddr>>24) - 2; // queue index for user: example: 192.168.1.2 is 0

	if(noslot) // only regular cb -> queue = 1
	{
		middle.arrival++;
		middle.heartbeat_arrival++;
		middle.cycle_arrival++;
		middle.heartbeat_processed++;
		middle.processed++;
		if(store)
		{
			store_total_arrival++;
			store_total_processed++;
		}
		heartbeat_total_arrival++;
		heartbeat_total_rate++;
		return nfq_set_verdict(qh, id, NF_ACCEPT, 0, NULL);  // how to treat the pkt after queue
	} else
	{
		// printf("send_cb called.\n");
		if(queue == 0) // packet from www: queue it
		{
			if (middle.active == 1)
			{
				if(ip->protocol == IPPROTO_TCP)
				{
					middle.arrival++;
					middle.heartbeat_arrival++;
					middle.cycle_arrival++;
					if (store)
						store_total_arrival++;
					heartbeat_total_arrival++;

					if(middle.q_len > QSIZE - 1)
					{
						total_loss++;
						middle.loss++;
						heartbeat_total_loss++;
						if (store)
							store_total_loss++;

						return nfq_set_verdict(qh, id, NF_DROP, 0, NULL);  // how to treat the pkt after queue

					}
					else
					{
						gettimeofday(&current, NULL);
						memcpy(middle.pkts[middle.tail_idx].pkt, buf, sizeof(buf));
						middle.pkts[middle.tail_idx].pkt_size = rv;
						middle.pkts[middle.tail_idx].pkt_tstamp = current;
						middle.pkts[middle.tail_idx].pkt_port = 0;
						middle.tail_idx = (middle.tail_idx + 1) % QSIZE;
						middle.q_len++;
						return nfq_set_verdict(qh, id, NF_STOLEN, 0, NULL);  // how to treat the pkt after queue
					}
				}
				else if (ip->protocol == IPPROTO_UDP)
				{
					udp = (struct udphdr *)(buf_data + sizeof(*ip));
					src_port = ntohs(udp->source);
					if (src_port == 53)
					{
						middle.arrival++;
						middle.heartbeat_arrival++;
						middle.cycle_arrival++;
						if (store)
							store_total_arrival++;

						heartbeat_total_arrival++;
						if(middle.q_len > QSIZE - 1)
						{
//							printf("Queue overflow!\n");
							total_loss++;
							middle.loss++;
							heartbeat_total_loss++;
							if (store)
								store_total_loss++;

							return nfq_set_verdict(qh, id, NF_DROP, 0, NULL);  // how to treat the pkt after queue
						} else
						{
							gettimeofday(&current, NULL);
						  	memcpy(middle.pkts[middle.tail_idx].pkt, buf, sizeof(buf));
							middle.pkts[middle.tail_idx].pkt_size = rv;
							middle.pkts[middle.tail_idx].pkt_tstamp = current;
							middle.pkts[middle.tail_idx].pkt_port = 0;
							middle.tail_idx = (middle.tail_idx + 1) % QSIZE;
							middle.q_len++;
							// jump start
							if (x_value <= 0 && active_traces == 0)
							{
								int new_traces = 1; // 10 / clients[dst_addr].strace_pcount + 1;
								x_value = new_traces;
								active_traces = new_traces;
								for (i = 0; i < new_traces ; i++)
								{
									current_sidx[i] = 0;
									trace[i] = 1;
								}
								err = 0;
								last_err = 0;
							}
//							printf("bad DNS\n");
							return nfq_set_verdict(qh, id, NF_STOLEN, 0, NULL);  // how to treat the pkt after queue
						}

					}
					else if (src_port == 5000)
					{
						middle.arrival++;
						middle.heartbeat_arrival++;
						middle.cycle_arrival++;
						if (store)
							store_total_arrival++;
						heartbeat_total_arrival++;

						if(middle.q_len > QSIZE - 1)
						{
							total_loss++;
							middle.loss++;
							heartbeat_total_loss++;
							if (store)
								store_total_loss++;

							return nfq_set_verdict(qh, id, NF_DROP, 0, NULL);  // how to treat the pkt after queue

						}
						else
						{
							gettimeofday(&current, NULL);
							memcpy(middle.pkts[middle.tail_idx].pkt, buf, sizeof(buf));
							middle.pkts[middle.tail_idx].pkt_size = rv;
							middle.pkts[middle.tail_idx].pkt_tstamp = current;
							middle.pkts[middle.tail_idx].pkt_port = 0;
							middle.tail_idx = (middle.tail_idx + 1) % QSIZE;
							middle.q_len++;
							return nfq_set_verdict(qh, id, NF_STOLEN, 0, NULL);  // how to treat the pkt after queue
						}

					}
					else
					{
						printf("unrecognized UDP packet queued up to userspace (%d): DROPPED.\n",src_port);
						return nfq_set_verdict(qh, id, NF_DROP, 0, NULL);  // how to treat the pkt after queue
					}
				}
				else
				{
					printf("unrecognized packet queued up to userspace: DROPPED.\n");
					return nfq_set_verdict(qh, id, NF_DROP, 0, NULL);  // how to treat the pkt after queue
				}
			}
			else
			{
//					printf("%d,%d,%d)\n", clients[dst_addr].q_len, clients[dst_addr].head_idx, clients[dst_addr].tail_idx);
				return nfq_set_verdict(qh, id, NF_DROP, 0, NULL);  // how to treat the pkt after queue
			}
		}
		else // read from queue and send it down tunnel
		{
			gettimeofday(&current, NULL);
			// remove from packet queue
			middle.head_idx = (middle.head_idx + 1) % QSIZE;
			middle.q_len--;

			return nfq_set_verdict(qh, id, NF_ACCEPT, 0, NULL);  // how to treat the pkt after queue
		}
	}
}

// nfq_handle_packet is called on queue 1 (udptunnel incoming packets): from mid-server: queue 1
static int recv_cb(struct nfq_q_handle *qh, struct nfgenmsg *nfmsg, struct nfq_data *nfa, void *data)
{
	struct timeval current;
  	u_int32_t id = 0;
  	struct nfqnl_msg_packet_hdr *ph = nfq_get_msg_packet_hdr(nfa);
	if (ph) {
		id = ntohl(ph->packet_id);
	}
	
	unsigned char * packet_data = NULL;
	if (nfq_get_payload(nfa, &packet_data) == -1)
	{
		printf("recv_cb nfq_get_payload error!\n");
	}
	struct iphdr *ip = (struct iphdr *) packet_data;
	struct udphdr *udp;
	uint16_t dst_port;
	gettimeofday(&current, NULL);
	if(ip->protocol == IPPROTO_TCP || ip->protocol == IPPROTO_UDP )
	{
		if (store)
		{
			fprintf(fp2,"1,%lu.06%lu\n", current.tv_sec % 10000, current.tv_usec);
			fflush(fp2);
			gettimeofday(&current, NULL);
		}
		return nfq_set_verdict(qh, id, NF_ACCEPT, 0, NULL); // how to treat the pkt after queue
	}
	else if (ip->protocol == IPPROTO_UDP)
	{
		udp = (struct udphdr *)(packet_data + sizeof(*ip));
		dst_port = ntohs(udp->dest);
		if (dst_port == 53 || dst_port == 5000)
		{
//			printf("DNS from client.\n");
			if (store)
			{
				fprintf(fp2,"1,%lu.06%lu\n", current.tv_sec % 10000, current.tv_usec);
				fflush(fp2);
				gettimeofday(&current, NULL);
			}
			return nfq_set_verdict(qh, id, NF_ACCEPT, 0, NULL);  // how to treat the pkt after queue
		}
		else
		{
			printf("unrecognized UDP packet queued up to userspace (%d): DROPPED.\n",dst_port);
			return nfq_set_verdict(qh, id, NF_DROP, 0, NULL);  // how to treat the pkt after queue
		}
	}
	else
	{
		printf("unrecognized packet queued up to userspace: DROPPED.");
		return nfq_set_verdict(qh, id, NF_DROP, 0, NULL);  // how to treat the pkt after queue
	}
}

int handle_middle_packets(int current_rate, int * packets_left)
{
	struct timeval current;
	int i = 0;

	pthread_mutex_lock(&lock);
	packets_left[0] -= MIN(middle.q_len,current_rate);
	int min_pcount = MIN(middle.q_len,current_rate);
	middle.processed += min_pcount; // middle.last_q_len - middle.q_len;
	middle.heartbeat_processed += min_pcount; // middle.last_q_len - middle.q_len;
	if (store)
		store_total_processed += min_pcount;
//	middle.arrival += middle.q_len - middle.last_q_len;
	middle.last_q_len = middle.q_len - min_pcount;
	pthread_mutex_unlock(&lock);

//	middle.last_q_len = middle.q_len; // temporarily set to keep track of queue before processing packets
	for (i = 0 ; i < min_pcount; i++)
	{
		pthread_mutex_lock(&lock);
		q_flg = 1; // called from inside: transmit packet (also client no.)
		gettimeofday(&current, NULL);
		struct timeval elapsed;
		timersub(&current,&(middle.pkts[middle.head_idx].pkt_tstamp),&elapsed);
		timeradd(&middle.delay,&elapsed,&middle.delay);
		memcpy(buf, middle.pkts[middle.head_idx].pkt, sizeof(buf));
		rv = middle.pkts[middle.head_idx].pkt_size;
		nfq_handle_packet(h, buf, rv); // parameters of cb
		pthread_mutex_unlock(&lock);
	}

//	printf("p_left %d.\n",*packets_left);

	return min_pcount;
}

void timer_cb(int sig)
{
	static int current_rate = 1;
	int current_slot = slot;
	int slot_rate = 0;
	int slot_cap = slot * MAX_RATE;


	int i = 0;
	struct timeval current;
	struct timeval before;
	int prob = 0;
	p_param = (int)(p * PRECISION);
	q_param = (int)(q * PRECISION);
	// long elapsed = 0;
	gettimeofday(&before,NULL);

	if (middle.active == 1)
	{
		//printf("hop\n");
		if(stick % heartbeat == 0 )
		{
// *			printf("arr: %f, strace: %f\n",avg_arrival,(double)strace_pcount/ strace_len);
			stick = stick % (heartbeat * current_slot * strace_len);
			hb_count++;
//			total_processed += middle.processed;

			middle.heartbeat_arrival = 0;
			middle.heartbeat_processed = 0;
			if (middle.ack_received == 0)
			{
				if (middle.missing_acks >= 3)
				{
					if (middle.first_time == 1)
					{
						middle.active = 0;
						middle.head_idx = 0;
						middle.tail_idx = 0;
						middle.q_len = 0;
//						begin = 0;
						middle.first_time = 0;
						printf("mid server left the channel.\n");

						if(store)
						{
							store = 0;
							gettimeofday(&current,NULL);
							struct timeval duration;
							timersub(&current,&store_begin_ts,&duration);
							printf("duration: %d, arrival: %d, rate: %d\n",
								store_total_slot,
								store_total_arrival,
								store_total_rate);
							printf("min_active: %d, active_bar: %f, max_active: %d\n",
								store_min_active,
								(double)store_total_active / (double)store_total_slot,
								store_max_active);
							printf("min_x: %d, x_bar: %f, max_x: %d\n",
								store_min_x,
								(double)store_total_x / (double)store_total_slot,
								store_max_x);
							printf("min_y: %f, y_bar: %f, max_y: %f\n",
								store_min_y,
								(double)store_total_y / (double)store_total_slot,
								store_max_y);
							printf("processed: %d, dummy: %d (%f), loss: %d (%f), delay: %f\n",
								store_total_processed,
								store_total_dummy,
								(double)store_total_dummy / (double)(store_total_dummy + store_total_processed),
								store_total_loss,
								(double)store_total_loss / (double)(store_total_arrival),
								store_total_delay / (double)store_total_processed);

							fprintf(fp,"%d,%d,%d,%f,%f,%d,%f,%d,%f\n",
								store_total_slot,
								store_total_arrival,
								store_total_rate,
								(double)store_total_x / (double)store_total_slot,
								(double)store_total_y / (double)store_total_slot,
								store_total_dummy,
								(double)store_total_dummy / (double)(store_total_dummy + store_total_processed),
								store_total_loss,
								store_total_delay / (double)store_total_processed);


							// fclose(fp);
							// fclose(fp2);
							// fclose(fp3);
						}
					}
				}
				else
				{
					middle.missing_acks++;	
					printf("ACK missed. Strike (%d).\n",middle.missing_acks);
					send_sync();
					gettimeofday(&current, NULL);
				}
			}
			else
			{
				if (middle.first_time == 0)
					middle.first_time = 1;
				middle.ack_received = 0;
				middle.missing_acks = 0;
				send_sync();
				gettimeofday(&current, NULL);
			}

			/* *printf("y_value = %f, x_value = %d, active_traces: %d, err: %f, trc: %d, arr: %d, srv: %d, q_len: %d, loss: %d\n",
					y_value,
					x_value,
					active_traces,
					err,
					strace_pcount,
					heartbeat_total_arrival,
					heartbeat_total_rate,
					middle.q_len,
					heartbeat_total_loss); */


			heartbeat_total_arrival = 0;
			heartbeat_total_rate = 0;
			heartbeat_total_loss = 0;
		}
		
		if (stick % 100 == 0)
		{
			fprintf(fp5,"%d\n",active_traces);
		}


	// is the stick beginning of current slot?
		if (stick % (strace_len * current_slot) == 0) // bergining of cycle: evaluate y
		{
			double temp_y = MIN((lambda - mu) * (double)strace_pcount / 2.0,(double)slot * MAX_RATE * (double)strace_len / (double)strace_pcount);
			avg_q_len /= strace_len * current_slot; // reconsider for other slots
			avg_lambda /= strace_len * current_slot; // reconsider for other slots
// *			printf("temp_y: %f, max_rate: %f, max_arrival: %d\n",temp_y,1.0 * (double)middle.cycle_arrival / strace_pcount,middle.cycle_arrival);
//			temp_y = MAX(temp_y,1.0 * (double)middle.cycle_arrival / strace_pcount);
			/* int temp_diff = (double)middle.cycle_arrival - cycle_rate;
			if (temp_diff > 0 && avg_q_len > temp_diff)
			{
				printf("%d > %d - %d\n",avg_q_len, middle.cycle_arrival , cycle_rate);
				temp_y = MIN(temp_y + 10.0 * ((double)(avg_q_len - temp_diff) / strace_pcount) , ((double)MAX_RATE * (double)strace_len / (double)strace_pcount));
			}
			else if (temp_diff < 0 && avg_q_len > 100)
			{
				printf("%d > 5\n",avg_q_len);
				temp_y = MIN(temp_y + 10.0 * ((double)avg_q_len / strace_pcount) , ((double)MAX_RATE * (double)strace_len / (double)strace_pcount));
			} */
			
/*			if (avg_q_len > lambda / alpha)
			{
				printf("%d > %f\n",avg_q_len, lambda / alpha);
//				temp_y = MIN(temp_y + 1.0 * ((double)(avg_q_len - lambda / alpha) / strace_pcount) , ((double)MAX_RATE * (double)strace_len / 				(double)strace_pcount));
//				temp_y = MIN(y_value + 1.0 * ((double)(avg_q_len - lambda / alpha) / strace_pcount) , ((double)MAX_RATE * (double)strace_len / (double)strace_pcount));
				temp_y = MAX(temp_y,temp_y + 10.0 * ((double)(avg_q_len - lambda / alpha) / strace_pcount));

			} */


//			printf("q_len: %d, exp_q: %f\n",temp_diff,lambda / alpha);
//			printf("temp_y: %d, max_arrival: %d\n",temp_y,max_arrival);
//			temp_y += ((double)avg_q_len - lambda / alpha) / strace_pcount;
			y_value = (1.0 - beta) * y_value + beta * temp_y;
//			y_value += ((double)temp_diff - lambda / alpha) / strace_pcount;
			min_q_len = QSIZE;
			avg_q_len = 0;
			avg_lambda = 0.0;
			max_arrival = 0;
			/* printf("temp_y: MIN( (%f - %f) * %d / 2 , %d * %d / %d) = %f\n",
										lambda,
										mu,
										strace_pcount,
										slot * MAX_RATE,
										strace_pcount,
										strace_len,
										temp_y);


			printf("qmax: MAX(1 , 2.0 * %f * 2 / %d / %f) = %f\n",
										y_value,
										strace_pcount,
										alpha,
										qmax); */
// *			printf("temp_y: %f, qmax: %f\n",temp_y,qmax);
			
			/* if (xy_diff > 0)
				x_value = y_value;
			else
				x_value = y_value +1;
			xy_diff += (double)x_value - y_value; */
			
			cycle_rate = 0;
			middle.cycle_arrival = 0;

		}

		if(stick % current_slot == 0)
		{
			/* if (err > 5000 || err < -5000)
			{
				err = 0;
				last_err = 0;
			} */
			min_q_len = MIN(min_q_len,middle.q_len);
			max_arrival = MAX(max_arrival,middle.last_slot_arrival);
			avg_arrival = avg_arrival * (1.0 - arr_beta) + arr_beta * middle.last_slot_arrival;
			avg_q_len += middle.q_len;
			avg_lambda += lambda;
			qmax = MAX(1.0,2.0 * y_value * 2.0 / ((double)strace_pcount * alpha));
//			lambda = MIN(MAX(0,lambda + alpha * (middle.last_slot_arrival - y_value * strace_pcount / strace_len)),qmax * alpha);
			mu = MAX(0,mu + alpha * (y_value * strace_pcount / strace_len - MAX_RATE * slot));
			
			// if (stick % current_slot * 100 == 0)
			{
				if (x_value <= 0 && active_traces == 0)
				{
					double thr = MIN((double)strace_pcount, (double)middle.q_len) / (double)strace_pcount / 100.0;
					thr *= PRECISION;
					prob = rand() % PRECISION;
					//if (prob < thr)
					if (avg_arrival > 5 * arr_beta)
					{
						// printf("NEW TRACE RANDOMLY JUMP STARTED! q_len: %d, prob: %d, thr: %f\n",middle.q_len,prob,thr);
						printf("NEW TRACE JUMP STARTED! arrival: %f, thr: %f\n",avg_arrival,(double)strace_pcount / (strace_len * 3));
						int new_traces = 1; // 10 / clients[dst_addr].strace_pcount + 1;
						x_value = new_traces;
						active_traces = new_traces;
						for (i = 0; i < new_traces ; i++)
						{
							current_sidx[i] = 0;
							trace[i] = 1;
						}
						err = 0;
						last_err = 0;
					}
				}
			}
						


			i = 0;
			int at = active_traces;
			rate = 0;

			while (at > 0) // sum up rates for this slot
			{
				for (; i < MAX_ACTIVE_TRACES ; i++)
				{
					if (trace[i] == 1)
					{
						rate += super_trace[current_sidx[i]];
						current_sidx[i] = (current_sidx[i] + 1) % strace_len;
						if (current_sidx[i] == 0)
						{
							trace[i] = 0;
							active_traces--;
						}
						at--;
						i++;

						gettimeofday(&current, NULL);
						/*
						if (store)
						{
							fprintf(fp,"1,%lu.%06lu,%d,%d,NaN,NaN,NaN,NaN,NaN\n",
								current.tv_sec % 10000,
								current.tv_usec,
								0,
								active_traces);
							fflush(fp);
						} */
						break;
					}
				}
			}

//			err += ((double)rate - y_value * (double)strace_pcount / strace_len);
//			err += ((double)rate - middle.last_slot_arrival);
	//		err += ((double)rate - MAX(avg_arrival,middle.last_slot_arrival));
			lambda = MAX(0,lambda + alpha * (MAX(avg_arrival,middle.last_slot_arrival) - (double)rate));
//			lambda = MAX(0,lambda + alpha * (middle.last_slot_arrival - (double)rate));

			
//			err += ((double)rate - MAX(y_value * (double)strace_pcount / strace_len, middle.last_slot_arrival));
			if(store)
			{
				fprintf(fp3,"%f,%d,%d,%12f,%d,%12f,%d,%f,%f\n",
					err,
					active_traces,
					x_value,
					y_value,
					rate,
					((double)strace_pcount / strace_len),
					middle.arrival,
					lambda,
					mu);
				fflush(fp3);
			}
			
			// int update_cycle = 100;
			if (stick % (update_cycle * current_slot) == 0) // bergining of cycle: evaluate y
			{
				int temp_x = 0;
				err = gamma_value - lambda;
//				err = avg_arrival * 1000 - lambda;
				if (err >= 0)
				{
					if (err >= last_err)
					{
						// temp_x = -1  * (int)(err / (update_cycle * strace_pcount / strace_len) + 0.5);
						temp_x = -1; //  * (int)(err / (update_cycle * strace_pcount / strace_len) - 0.5);
						// printf("   HAHAHAH   %d, %f\n",temp_x,err);
						last_err = err;
					}
				}
				else
				{
					if (err < last_err)
					{
						temp_x = 1; // * (int)(-err / (update_cycle * strace_pcount / strace_len) - 0.5);
						// printf("   HAHAHAH   %d, %f\n",temp_x,err);
						last_err = err;
					}

				}
				x_value = MAX(0,MIN(MAX_ACTIVE_TRACES,temp_x + x_value));
			}

/*			if (stick % (100 * current_slot) == 0) // bergining of cycle: evaluate y
			{
				if (err > sigma / 4)
				{
					if (err > last_err)
					{
						x_value = MAX(0,x_value - 1);
						// x_value = MIN(MAX(0,x_value - 1),err * strace_len / strace_pcount);
						last_err = err;
					}
				}
				else if (err < - sigma / 4)
				{
					if (err <= last_err)
					{
						x_value = MIN(MAX_ACTIVE_TRACES, x_value + 1);
						// x_value = MIN(MIN(MAX_ACTIVE_TRACES, x_value + 1),err * strace_len / strace_pcount);
						last_err = err;
					}

				}
			} */

			int req_trace_count = MIN(MAX_ACTIVE_TRACES,x_value) - active_traces;
//						printf("a trace ended: %d new traces will be added. x_value: %d, ac: %d\n",MAX(0,req_trace_count), clients[current_client].x_value, clients[current_client].active_traces);
			i = 0;
			while (req_trace_count > 0)
			{
				for (; i < MAX_ACTIVE_TRACES; i++)
				{
					if (trace[i] == 0)
					{
						trace[i] = 1;
						active_traces++;
						current_sidx[i] = 0;
						gettimeofday(&current, NULL);
						/*
						if (store)
						{
							fprintf(fp,"1,%lu.%06lu,%d,%d,NaN,NaN,NaN,NaN,NaN\n",
								current.tv_sec % 10000,
								current.tv_usec,
								0,
								active_traces);
							fflush(fp);
						} */
						req_trace_count--;
						i++;
						break;
					}
				}
			}

			slot_rate = MIN((int)((double)rate / (1.0 - epsilon)),slot_cap);
			if (store)
				store_total_rate += slot_rate;
			cycle_rate += slot_rate;
			if(!noslot)
				heartbeat_total_rate += slot_rate;
			current_rate = MIN((int)((double)rate / (1.0 - epsilon)),slot_cap);

			prob = rand() % PRECISION;
			if (prob < p_param)
			{
				total_rate += current_rate;
				handle_middle_packets(MIN(current_rate,slot_rate),&slot_rate);
				// prepare for while: the reason we use flag instead of q_len is that q_len might change during this cycle by receiving more packets
				middle.flag = middle.q_len;

				if (begin) // dummy rate is allowed
				{
					int temp_len = slot_rate;
//					re-use dummy woth real traffic
//					printf("entered loop\n");
					if(middle.q_len > 0)
					{
						middle.reused += handle_middle_packets(MIN(current_rate,slot_rate), &slot_rate);
//						middle.flag-= 0;
					}

					total_reused += temp_len - slot_rate;
//					printf("left loop %d\n",temp_len - slot_rate);
					// no packet queued dummy for rest
					for (i = 0; i < slot_rate; i++)
					{
						gettimeofday(&current, NULL);
						// how does it know who to send dummy to !?
			/* reconsider */		sendto(sockfd, msg_dummy, 13, 0,  
							(struct sockaddr *)&middle.addr, sizeof(middle.addr));
//						clients[current_client].loss++;
						total_dummy++;
						if (store)
							store_total_dummy++;

//						prob = rand() % PRECISION;
						// printf("prob: %d,\tq_param: %d\n", prob,q_param);
//						if( prob < q_param)
//						{	// send dummy packet
//
					}
				}
			}

			if (store)
			{
				store_min_active = MIN(active_traces,store_min_active);
				store_total_active += active_traces;
				store_max_active = MAX(active_traces,store_max_active);
				store_min_x = MIN(x_value,store_min_x);
				store_total_x += x_value;
				store_max_x = MAX(x_value,store_max_x);
				store_min_y = MIN(y_value,store_min_y);
				store_total_y += y_value;
				store_max_y = MAX(y_value,store_max_y);
				store_total_slot++;

				gettimeofday(&current, NULL);
				fprintf(fp,"0,%lu.%06lu,%d,%d,%d,%d,%d,%lu.%06lu,%d\n",
					current.tv_sec % 10000,
					current.tv_usec,
					0,
					middle.arrival,
					middle.processed,
					middle.reused,
					middle.q_len,
					middle.delay.tv_sec,
					middle.delay.tv_usec,
					middle.loss);
				fflush(fp);
				store_total_delay += (double)middle.delay.tv_sec +
						 (double)middle.delay.tv_usec / 1000000.0;
				// for multi-user change x_value and y_value to the sum
				fprintf(fp,"0,%lu.%06lu,-1,%d,%d,%d,%d,%d,%d\n",
					current.tv_sec % 10000,
					current.tv_usec,
					MIN(rate,slot_cap),
					total_dummy,
					total_reused,
					active_traces,
					x_value,
					middle.processed
					);
				fflush(fp);
			}

			middle.last_slot_arrival = middle.arrival;
			middle.delay.tv_sec = 0;
			middle.delay.tv_usec = 0;
			middle.arrival = 0;
			middle.loss = 0;
			middle.processed = 0;
			total_rate = 0;
			middle.flag = 0;
			middle.reused = 0;

			total_dummy = 0;
			total_loss = 0;
			total_reused = 0;
			gettimeofday(&current, NULL);
			timersub(&current,&before,&current);
//			if (current.tv_usec > clients[0].slot * 1000)
//				printf("slot processing took long!\n");
//			printf("slot took: %lu.%06lu.\n",current.tv_sec,current.tv_usec);


		}
	}

	stick++;
}

void* handle_middle_socket()
{
	int i;
	struct timeval current;
	struct sockaddr_in cliaddr;
	int sockrv = 0;
	int addrlen = sizeof(cliaddr);
	char sockbuf[PSIZE_MAX] __attribute__ ((aligned));
	for(;;)
	{
		if ( ( sockrv = recvfrom(sockfd, sockbuf, sizeof(sockbuf), 0, (struct sockaddr *)&cliaddr, (socklen_t *)&addrlen ) ) >= 0) {
//			printf("%08x, %d\n",cliaddr.sin_addr.s_addr, dst_addr);
			switch (sockbuf[0])
			{
				case PKT_HELLO:
					y_value = 0.0;
					alpha = 0.001;
					pthread_mutex_lock(&lock);
					middle.first_time = 0;
//					middle.begin = predefined_begin;
					middle.id = 0;
					middle.addr = cliaddr;

//					middle.slot = predefined_slot;
					rate = 0;
					middle.ack_received = 0;
					middle.active = 1;
					middle.head_idx = 0;
					middle.tail_idx = 0;
					middle.q_len = 0;
					middle.last_q_len = 0;
					min_q_len = QSIZE;
					avg_q_len = 0;
					avg_lambda = 0.0;
					max_arrival = 0;
					current_sidx[0] = 0;
					for (i = 0 ; i < MAX_ACTIVE_TRACES ; i++)
						trace[i] = 0;
					active_traces = 0;

					middle.delay.tv_sec = 0;
					middle.delay.tv_usec = 0;
					middle.reused = 0;
					middle.processed = 0;
					middle.heartbeat_processed = 0;
					middle.arrival = 0;
					middle.heartbeat_arrival = 0;
					middle.cycle_arrival = 0;
					middle.loss = 0;
					heartbeat_total_arrival = 0;
					heartbeat_total_rate = 0;
					heartbeat_total_loss = 0;
					middle.last_slot_arrival = 0;
					last_slot_total_arrival = 0;
//					middle.silent = 1;
					middle.flag = 0;
					middle.missing_acks = 0;
					
					int arr = -1;
					memcpy(&arr, sockbuf + 9, sizeof(arr));
					short pwr = -1;
					memcpy(&pwr, sockbuf + 6, sizeof(pwr));
					memcpy(&test_num, sockbuf + 14, sizeof(test_num));
					alpha = 1.0;
					
					// read_strace2(arr,pwr, super_trace, &strace_len, &strace_pcount);
					// stick = 0;

//					for (i = 0; i < pwr; i++)
//						alpha /= 10.0;
					// gamma_value = pwr;
					y_value = (double)arr / ((double)strace_pcount * 1000.0 / (double)strace_len);
					x_value = 0;
					err = 0.0;
//					printf("test: %d, pow: %d, alpha: %f, arrival: %d, y_value: %f\n",test_num, pwr,alpha,arr,y_value);

					// optimization variables
//					x_value = 0.0;
	//				y_value = 1.0;
					update_cycle = arr;
					sigma = 1.0;
	//				alpha = 0.01; //1.0 / clients[dst_addr].strace_pcount;
					beta = 0.1;
					arr_beta = 1.0 / 1000.0;
//					arr_beta = 1.0 / (double)strace_len * slot;
					gamma_value = pwr;
					epsilon = 0.0;
					err = 0;
					avg_arrival = 0.0;
					last_err = 0;
					lambda = 0;
					mu = 0;

					printf("mid point joined %d.\n",middle.active);
					printf("test: %d, arr: %d, pwr: %d, alpha: %f, cycle: %d, gamma: %f\n",test_num, arr,pwr,alpha,update_cycle,gamma_value);

					send_sync();
					gettimeofday(&current, NULL);

					if (store)
					{
						// fclose(fp);
						// fclose(fp2);
						// fclose(fp3);
						// fclose(fp5);
						store = 0;
					}
					// printf("store: %d\n",store);
					if (store == 0)
					{
						store_y_value = y_value;
						store_alpha = alpha;
						store_total_rate = 0;
						store_total_arrival = 0;
						store_total_processed = 0;
						store_total_delay = 0.0;
						store_total_dummy = 0;
						store_total_loss = 0;
						store_min_active = MAX_ACTIVE_TRACES + 1;
						store_total_active = 0;
						store_max_active = -1;
						store_min_x = MAX_ACTIVE_TRACES + 1;
						store_total_x = 0;
						store_max_x = -1;
						store_min_y = MAX_ACTIVE_TRACES + 1;
						store_total_y = 0;
						store_max_y = -1;
						store_total_slot = 0;
						gettimeofday(&store_begin_ts,NULL);
						
//						char *pos;
//						if ((pos=strchr(filename, '\n')) != NULL)
//							*pos = '\0';
						sprintf(path,"logs/test-%d-up.log",test_num);
						fp = fopen(path,"w");
						sprintf(path,"logs/test-%d-down.log",test_num);
						fp2 = fopen(path,"w");
						sprintf(path,"logs/test-%d-err.log",test_num);
						fp3 = fopen(path,"w");
						sprintf(path,"logs/test-%d-ac.log",test_num);
						fp5 = fopen(path,"w");
						sprintf(path,"logs/test-results.log");
						fp4 = fopen(path,"a");
						store = 1;
					}
					printf("y_value: %f, alpha: %f\n",store_y_value,store_alpha);
					pthread_mutex_unlock(&lock);
// 					printf("(%d) %lu.%06lu SYNC sent.\n",dst_addr, current.tv_sec,current.tv_usec);
					break;
				case PKT_ACK:
					gettimeofday(&current, NULL);
					pthread_mutex_lock(&lock);
					middle.ack_received = 1;
					pthread_mutex_unlock(&lock);
					
// 					printf("(%d) %lu.%06lu ACK received\n",dst_addr, current.tv_sec,current.tv_usec);
					break;
				case PKT_WRAP:
					printf("WRAP MSG RECEIVED.\n");
					pthread_mutex_lock(&lock);
					fprintf(fp4,"%d,%.1f,%f,%d,%d,%d,%d,%d, %d,%f,%d, %d,%f,%d, %f,%f,%f, %d,%d,%f, %d,%f,%f\n",
						test_num,
						store_y_value,
						store_alpha,
						strace_pcount,
						strace_len,
						store_total_slot,
						store_total_arrival,
						store_total_rate,
						store_min_active,
						(double)store_total_active / (double)store_total_slot,
						store_max_active,
						store_min_x,
						(double)store_total_x / (double)store_total_slot,
						store_max_x,
						store_min_y,
						(double)store_total_y / (double)store_total_slot,
						store_max_y,
						store_total_processed,
						store_total_dummy,
						(double)store_total_dummy / (double)(store_total_dummy + store_total_processed),
						store_total_loss,
						(double)store_total_loss / (double)(store_total_arrival),
						store_total_delay / (double)store_total_processed);					
					fclose(fp);
					fclose(fp2);
					fclose(fp3);
					fclose(fp4);
					fclose(fp5);
					
					char path_ac[100];
					sprintf(path_ac,"logs/test-%d-ac.log",test_num);
					fp5 = fopen(path_ac,"r");
					int prev_ac = 0;
					int current_dur = 0;
					char *line_ac = NULL;
					size_t len_ac = 0;
					ssize_t read_ac;

					while ((read_ac = getline(&line_ac, &len_ac, fp5)) != -1)
					{
						int current_ac = atoi(line_ac);
						if (current_ac != prev_ac)
						{
							printf("%d:%d,",prev_ac,current_dur);
							current_dur = 1;
						}
						else
						{
							current_dur++;
						}
						prev_ac = current_ac;
					}
					printf("%d:%d,",prev_ac,current_dur);
					printf("\n");
					fclose(fp5);
					free(line_ac);
					pthread_mutex_unlock(&lock);
					break;
				case PKT_RATE:
					pthread_mutex_lock(&lock);

					arr = -1;
					memcpy(&arr, sockbuf + 8, sizeof(arr));
					pwr = -1;
					memcpy(&pwr, sockbuf + 5, sizeof(pwr));
					alpha = 1.0;
					for (i = 0; i < pwr; i++)
						alpha /= 10.0;
					y_value = (double)arr / ((double)strace_pcount * 1000.0 / (double)strace_len);
					x_value = y_value;
					err = 0.0;
					printf("pow: %d, alpha: %f, arrival: %d, y_value: %f\n",pwr,alpha,arr,y_value);
					pthread_mutex_unlock(&lock);
				case PKT_DUMMY:
//					printf("dummy received and ignored.\n");
					break;
				default:
					printf("unknown UDP packet: dropped.\n");
					break;
			}
			continue;
		}
		if (sockrv < 0 && errno == ENOBUFS) {
			printf("losing packets!\n");
			continue;
		}
		perror("recv failed");
		break;
	}
	return NULL;
}

// thread for reading packets from udp tunnel
void* handle_packet()
{
	char tmpbuf[PSIZE_MAX] __attribute__ ((aligned));
	int tmprv = -1;
	for(;;)
	{
		if ((tmprv = recv(fd, tmpbuf, sizeof(tmpbuf), 0)) >= 0) {
			pthread_mutex_lock(&lock);
			memcpy(buf,tmpbuf,sizeof(buf));
			rv = tmprv;
			q_flg = 0; // called from outside: queue the packet
			nfq_handle_packet(h, buf, rv); // parameters of cb
			// printf("%d %d\n",sizeof(buf), rv);
			pthread_mutex_unlock(&lock);
			continue;
		}
		if (rv < 0 && errno == ENOBUFS) {
			printf("losing packets!\n");
			continue;
		}
		perror("recv failed");
		break;
	}
	return NULL;
}

void *handle_clock()
{
	sigset_t waitset;
	struct itimerval timer;
	int sig;
	int result = 0;

	sigemptyset(&waitset);
	sigaddset(&waitset, SIGALRM);
	// sigprocmask(SIG_BLOCK, &waitset, NULL);

	timer.it_value.tv_sec = 0;//waits for 5 seconds before sending timer signal
	timer.it_value.tv_usec = 1000;
	timer.it_interval.tv_sec = 0;//sends timer signal every 5 seconds
	timer.it_interval.tv_usec = 1000;

	setitimer(ITIMER_REAL, &timer, NULL);

	while(1)
	{
		//printf("before stick %u\n",stick);
		result = sigwait(&waitset, &sig);
		//printf("after stick %u\n",stick);
		if(result == 0) // success
			timer_cb(sig);
		else
		{
			printf("sigwait() returned error number %d\n", errno);
			perror("sigwait() function failed\n");
		}
	}
	printf("we got here.\n");
}



int main(int argc, char** argv)
{
	int i = 0;
	int rset = 0;
	slot = 1;
	rate = 2147483647;
	middle.active = 0;
	read_strace(slot, super_trace, &strace_len, &strace_pcount);
	printf("trace loaded. len: %d, pcount: %d\n",strace_len,strace_pcount);


	{
		q = 1.0; // atof(argv[++i]);
		begin = 1;
	}

	for (i = 1 ; i < argc; i++)
	{
		if(!strcmp(argv[i],"-s"))
			slot = atoi(argv[++i]);
		else if(!strcmp(argv[i],"-p"))
		{
			p = atof(argv[++i]);
		}
		//else if(!strcmp(argv[i],"-q"))
		else if(!strcmp(argv[i],"-r"))
		{
			rate = atoi(argv[++i]);
			rset = 1;
		}
		else if(!strcmp(argv[i],"-tr"))
			toggle_rate = atoi(argv[++i]);
		else if(!strcmp(argv[i],"-adaptive"))
			adaptive = 1;
		else if(!strcmp(argv[i],"-debug"))
			debug = 1;
		else if(!strcmp(argv[i],"-noslot"))
			noslot = 1;
		else if(!strcmp(argv[i],"-store"))
		{
			sprintf(filename,"%s",argv[++i]);
			store = 1;
		}
		else
		{
			printf("Usage: udpserver [OPTION]\n");
//			printf("  -s,\t\t slot size in milliseconds - default: 1\n");
//			printf("  -q,\t\t probability of sending dummy packets - default: 0\n");
//		printf("  -l,\t\t length of cycle - default: slot size\n");
//			printf("  -r,\t\t packets per slot - default: max\n");
			printf("  -t,\t\t total cycle length as multiplier of small cycle - default: small cycle\n");
			printf("  -tr,\t\t packet rate to toggle (in adaptive cycle change) - default: Inf\n");
			printf("  -noslot,\t no time slotting\n");
			printf("  -adaptive,\t adaptive cycle change\n");
			printf("  -debug,\t run in debug mode\n");
			printf("  -store,\t store timestamps\n");
			return 1;
		}
	}

	if(noslot)
		begin = 0;
	if(!rset)
	{
		if (begin)
		{
			printf("high cycle rate is set to 10 packets by default\n");
			rate = 1;
		}
		else
			rate = slot * 1000 / 50;
	}
	int bad_input = 0;
	if(slot * 1000 / 50 < rate)
	{
		printf("Input Error: high cycle rate higher than capacity.\n");
		printf("high cycle rate: %d, max rate: %d\n",rate,(slot * 1000 / 50));
		bad_input = 1;
	}
	if(bad_input)
	{
		printf("For instructions type: udpclient -?\n");
		return 1;
	}
	printf("slot = %d(ms), rate = %d\n", slot, rate);
	
	char *pos;
	if ((pos=strchr(filename, '\n')) != NULL)
		*pos = '\0';
	sprintf(path,"logs/test-results.log");
	fp4 = fopen(path,"w");
	fclose(fp4);
	
	if(store)
	{
		store_total_rate = 0;
		store_total_arrival = 0;
		store_total_processed = 0;
		store_total_delay = 0.0;
		store_total_dummy = 0;
		store_total_loss = 0;
		store_min_active = MAX_ACTIVE_TRACES + 1;
		store_total_active = 0;
		store_max_active = -1;
		store_min_x = MAX_ACTIVE_TRACES + 1;
		store_total_x = 0;
		store_max_x = -1;
		store_min_y = MAX_ACTIVE_TRACES + 1;
		store_total_y = 0;
		store_max_y = -1;
		store_total_slot = 0;
		gettimeofday(&store_begin_ts,NULL);
		char *pos;
		if ((pos=strchr(filename, '\n')) != NULL)
		    *pos = '\0';
		sprintf(path,"logs/%s-up.log",filename);
		fp = fopen(path,"w");
		sprintf(path,"logs/%s-down.log",filename);
		fp2 = fopen(path,"w");
		sprintf(path,"logs/%s-err.log",filename);  
		fp3 = fopen(path,"w");
	}

	sockfd = socket(AF_INET, SOCK_DGRAM, 0);
	
	bzero(&servaddr, sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
	servaddr.sin_port = htons(47380);
	
	bind(sockfd, (struct sockaddr *)&servaddr, sizeof(servaddr));
	
// NET_FILTER Initialization
	printf("opening library handle\n");
	h = nfq_open();
	if (!h) {
		fprintf(stderr, "error during nfq_open()\n");
		exit(1);
	}

	printf("unbinding existing nf_queue handler for AF_INET (if any)\n");
	if (nfq_unbind_pf(h, AF_INET) < 0) {
		fprintf(stderr, "error during nfq_unbind_pf()\n");
		exit(1);
	}

	printf("binding nfnetlink_queue as nf_queue handler for AF_INET\n");
	if (nfq_bind_pf(h, AF_INET) < 0) {
		fprintf(stderr, "error during nfq_bind_pf()\n");
		exit(1);
	}

	printf("binding 'send to client' socket to queue '0'\n");
	sqh = nfq_create_queue(h,  0, &send_cb, &q_flg); // cb will act as callback
	if (!sqh) {
		fprintf(stderr, "error during nfq_create_queue()\n");
		exit(1);
	}
	printf("binding 'receive from client' socket to queue '1'\n");
	rqh = nfq_create_queue(h,  1, &recv_cb, NULL); // cb will act as callback
	if (!rqh) {
		fprintf(stderr, "error during nfq_create_queue()\n");
		exit(1);
	}
	printf("setting copy_packet mode\n");
	if ((nfq_set_mode(sqh, NFQNL_COPY_PACKET, 0xffff) < 0) || (nfq_set_mode(rqh, NFQNL_COPY_PACKET, 0xffff) < 0)) {
		fprintf(stderr, "can't set packet_copy mode\n");
		exit(1);
	}
	printf("setting queue_maxlen\n");
	if ((nfq_set_queue_maxlen(sqh, FILTER_QSIZE) < 0) || (nfq_set_queue_maxlen(rqh, FILTER_QSIZE) < 0)) {
		fprintf(stderr, "can't set queue_maxlene\n");
		exit(1);
	}

	printf("setting buffer_size\n");
	if ((nfnl_rcvbufsiz(nfq_nfnlh(h), 1500 * FILTER_QSIZE) < 0)) {
		fprintf(stderr, "can't set buffer_size\n");
		exit(1);
	}
	
	fd = nfq_fd(h);
	
	pthread_t send_thread;
	pthread_t read_thread;
	pthread_t clock_thread;
	pthread_t socket_thread;

	// struct sigaction sa;
	sigset_t newset;
	sigemptyset (&newset);
	sigaddset(&newset, SIGALRM);
	sigprocmask( SIG_BLOCK, &newset, NULL);

	pthread_mutex_init(&lock, NULL);
	
	// pthread_create(&heartbeat_thread, NULL, handle_heartbeat, NULL);
	pthread_create(&send_thread, NULL, handle_packet, NULL);
	pthread_create(&read_thread, NULL, read_input, NULL);
	pthread_create(&clock_thread, NULL, handle_clock, NULL);
	pthread_create(&socket_thread, NULL, handle_middle_socket, NULL);
	
	// pthread_join(heartbeat_thread, NULL);
	pthread_join(send_thread, NULL);
	pthread_join(read_thread, NULL);
	pthread_join(clock_thread, NULL);
	pthread_join(socket_thread, NULL);
	
	pthread_mutex_destroy(&lock);

	printf("unbinding from queue 0\n");
	nfq_destroy_queue(sqh);
	printf("unbinding from queue 1\n");
	nfq_destroy_queue(rqh);


	#ifdef INSANE
	/* normally, applications SHOULD NOT issue this command, since
	 * it detaches other programs/sockets from AF_INET, too ! */
	printf("unbinding from AF_INET\n");
	nfq_unbind_pf(h, AF_INET);
#endif
	printf("closing library handle\n");
	nfq_close(h);
	exit(0);
}
