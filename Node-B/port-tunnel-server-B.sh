#!/bin/bash
sudo iptables -F
sudo iptables -t nat -F
sudo iptables -t raw -F
sudo sysctl -w net.ipv4.ip_forward=1
sudo iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
sudo iptables -A FORWARD -i eth0 -o eth2 -m state --state RELATED,ESTABLISHED -p tcp -j NFQUEUE --queue-num 0
sudo iptables -A FORWARD -i eth0 -o eth2 -m state --state RELATED,ESTABLISHED -p udp --source-port 53 -j NFQUEUE --queue-num 0
sudo iptables -A FORWARD -i eth0 -o eth2 -m state --state RELATED,ESTABLISHED -j ACCEPT
sudo iptables -A FORWARD -i eth2 -o eth0 -p tcp -j NFQUEUE --queue-num 1
sudo iptables -A FORWARD -i eth2 -o eth0 -p udp --destination-port 53 -j NFQUEUE --queue-num 1
sudo iptables -A FORWARD -i eth2 -o eth0 -j ACCEPT
# sudo iptables -t raw -A PREROUTING -p udp --destination-port 47380 -j NFQUEUE --queue-num 1





