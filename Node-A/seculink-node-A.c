// current_client : start from zero and merge with client id

#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
//#include <netinet/tcp_seq.h>

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>

#include <linux/types.h>
#include <linux/netfilter.h>		/* for NF_ACCEPT */
#include <errno.h>
#include <libnetfilter_queue/libnetfilter_queue.h>
#include <sys/time.h>
#include <pthread.h>
#include <signal.h>


#include <netinet/tcp.h>
#include <netinet/udp.h>
#include <arpa/inet.h>

#define MAX_ACTIVE_TRACES 500
#define MAX_RATE 20
#define PSIZE_MAX 2048
#define QSIZE 200000
#define FILTER_QSIZE 200000
#define CLIENTS 2
#define PRECISION 100000
#define MAX(A,B) (((A) >= (B)) ? (A) : (B))
#define MIN(A,B) (((A) < (B)) ? (A) : (B))
#define ABS(A) (((A) > 0) ? (A) : (-A))

// packet types
#define PKT_TCP 'E'
#define PKT_DUMMY 'D'
#define PKT_HELLO 'H'
#define PKT_BEGIN 'B'
#define PKT_END 'F'
#define PKT_ACK 'A'
#define PKT_SYNC 'S'
#define PKT_RATE 'R'
#define PKT_WRAP 'W'

extern int errno;


//Socket variables
int sockfd, sockfd2, n;
short is_hello = 1;
short ack_received = 0;
struct sockaddr_in servaddr, servaddr2;

socklen_t len;
char msg_hello[16] = "";
char msg_wrap[5] = "WRAP\n";
char msg_dummy[13] = "DUMMY PACKET\n";
char msg_sync[14] = "";
char msg_ack[4] = "ACK\n";
char msg_rate[12] = "";

char buf[PSIZE_MAX] __attribute__ ((aligned));

// Netfilter queue variables
int rv;
struct nfq_handle *h;
struct nfq_q_handle *sqh, *rqh;
struct nfnl_handle *nh;
int fd;
short test_num = 0;
FILE *fp = 0;
FILE *fp2 = 0;
FILE *fp3 = 0;
FILE *fp4 = 0;
FILE *fp5 = 0;
char filename[100] = "packets.txt";
char path[200] = "/";

// Hazard avoidance variables
pthread_mutex_t lock;
short q_flg = 0;
char send_buf[PSIZE_MAX] __attribute__ ((aligned));
int send_size;
short first_send = 1;
short begin = 0;
int debug = 0;
int noslot = 0;
int dynamic = 0;
short store = 0;

int store_total_rate;
int store_total_arrival;
int store_total_processed;
double store_total_delay;
int store_total_dummy;
int store_total_loss;

int store_min_active;
int store_total_active;
int store_max_active;

int store_min_x;
int store_total_x;
int store_max_x;

double store_min_y;
double store_total_y;
double store_max_y;

int heartbeat_total_arrival;
int heartbeat_total_rate;
int heartbeat_total_loss;

int store_total_slot;

double store_y_value = -1.0;
double store_alpha = -1.0;
struct timeval store_begin_ts;
long hb_count = 0;
int total_dummy = 0;
int total_loss = 0;
// int total_processed = 0;
// int report_rate = 0;
int total_reused = 0;

double xy_diff = 0;
double qmax = 10000000.0;

struct packet
{
	char pkt[PSIZE_MAX];
	int pkt_size;
	uint16_t pkt_port;
	struct timeval pkt_tstamp;
};

int last_added_id = -1;
int first_added_id = -1;
struct trace
{
	int next_id;
	int prev_id;
	int idx;
	int active;
};

int update_cycle = 100;
int slot;
int rate;
short super_trace[40000];
int strace_len;
int strace_pcount;
int active_traces;
int current_sidx[MAX_ACTIVE_TRACES];
int trace[MAX_ACTIVE_TRACES]; // array of traces: 1 is active 0 means inactive
int total_rate;
// optimization variables
int x_value;
double y_value;
double sigma;
double alpha;
double beta;
double arr_beta;
double gamma_value = 1024.0;
double epsilon;
double err;
int last_err;
double lambda;
double mu;
int min_q_len;
int avg_q_len;
int max_arrival = 0;
double avg_lambda;
int last_slot_total_arrival;
double avg_arrival;
int cycle_rate;


struct client
{
	int id;
	struct sockaddr_in addr;
	int next_id;
	int prev_id;

	// packet queue
	struct packet pkts[QSIZE];
	int head_idx;
	int tail_idx;
	int q_len;
	int last_q_len;

//	int slot;
//	int rate;
	short ack_received;
	short first_time;
	short active;

/*	short super_trace[40000];
	int strace_len;
	int strace_pcount;
	int active_traces;
	int current_sidx[MAX_ACTIVE_TRACES];
	int trace[MAX_ACTIVE_TRACES]; // array of traces: 1 is active 0 means inactive */

	struct timeval delay;
	int reused;
	int arrival;
	int heartbeat_arrival;
	int loss;
	int last_slot_arrival;
	int processed;
	int heartbeat_processed;
//	int total_rate;
	short flag;

	int missing_acks;

/*	// optimization variables
	int x_value;
	double y_value;
	double sigma;
	double alpha;
	double beta;
	double gamma;
	double epsilon;
	double err;
	int last_err;
	double lambda;
	double mu; */
	int cycle_arrival;
//	int cycle_rate;

//	int acks;
//	int syns;
};

// Time slotting variables

int clients_size = CLIENTS;
struct client clients[CLIENTS];

short active_clients = 0;
short current_client = 0;

double p = 1.0;
double q = 1.0;
int p_param = 0;
int q_param = 0;
int power_value = 1;
int heartbeat = 1000;
int slack = 0;
// int tick = 0;
int stick = 0;

short fetch = 0;
struct timeval fetch_begin;
struct timeval sample_begin;

void send_sync(int current)
{
	msg_sync[0] = 'S';
	msg_sync[1] = 'Y';
	msg_sync[2] = 'N';
	msg_sync[3] = 'C';
	msg_sync[4] = ' ';

	int tmp = stick;
	msg_sync[5] = (tmp >> 24) & 0xFF;
	msg_sync[6] = (tmp >> 16) & 0xFF;
	msg_sync[7] = (tmp >> 8) & 0xFF;
	msg_sync[8] = tmp & 0xFF;

	sendto(sockfd, msg_sync, 9, 0, (struct sockaddr *)&clients[current].addr, sizeof(clients[current].addr));
	return;
}

void print_packet(unsigned char *packet_data, int size)
{
	struct iphdr *ip;
	struct tcphdr *tcp;
	struct udphdr *udp;	
	struct in_addr ipa;
	char src_ip[20];
	char dst_ip[20];
	uint16_t src_port;
	uint16_t dst_port;
	
	ip = (struct iphdr *) packet_data;
	ipa.s_addr=ip->saddr;
	strcpy (src_ip, inet_ntoa(ipa));
	ipa.s_addr=ip->daddr;
	strcpy (dst_ip, inet_ntoa(ipa));

	if(ip->protocol == IPPROTO_UDP) {
		udp = (struct udphdr *)(packet_data + sizeof(*ip));
		src_port = ntohs(udp->source);
		dst_port = ntohs(udp->dest); //th_dport);
		fprintf(stdout, "UDP: %s(%u) --> %s(%u) - p_size: %d\n", src_ip, src_port, dst_ip, dst_port,size);
	} else if (ip->protocol == IPPROTO_TCP)
	{
		tcp = (struct tcphdr *)(packet_data + sizeof(*ip));
		src_port = ntohs(tcp->th_sport);
		dst_port = ntohs(tcp->th_dport);
		fprintf(stdout, "TCP: %s(%u) --> %s(%u) - p_size: %d\n", src_ip, src_port, dst_ip, dst_port,size);
	}
	return;
}

int packet_type(unsigned char *data, int size)
{
	unsigned char type = (unsigned char)*data;
	switch(type)
	{
		case 'E': // tcp packet
			return 0;
		case 'D': // dummy packet
			return 1;
		case 'B': // begin
			return 2;
		case 'F': // finish
			return 3;
		default: // nothing
			return -1;
	}
	//show_tcp(data,size);
}

void read_strace(int slot, short* trace, int * tlen, int * pcount)
{

	char path[100];
	pcount[0] = 0;
	char *line = NULL;
	size_t len = 0;
	ssize_t read;
	sprintf(path,"strace-%d.trc",slot);
	FILE * fp = fopen(path,"r");
	tlen[0] = 0;
//	int max_rate = -1;
//	short temp_trace[1000 / slot];
//	int temp_count = 0;
//	int temp_len = 0;
//	printf("sec_split: ");
	while ((read = getline(&line, &len, fp)) != -1)
	{
		trace[tlen[0]] = atoi(line);
		pcount[0] += (double)trace[tlen[0]];
		tlen[0]++;

/*		temp_trace[temp_len] = atoi(line);
		temp_count += temp_trace[temp_len];
		temp_len++;

		if(temp_len == 1000) // begining of a second
		{
			if (temp_count > max_rate)
			{
				int i;
				for(i = 0 ; i < 1000 / slot; i++)
					trace[i] = temp_trace[i];
				pcount[0] = temp_count;
				tlen[0] = 1000 / slot;
				max_rate = temp_count;
			}
			printf("%d, ",temp_count);
			temp_count = 0;
			temp_len = 0;
		} */
	}
	fclose(fp);
	printf("\npcount: %d, len: %d, avg: %f\n",pcount[0],tlen[0],((double)pcount[0] / tlen[0]));
	if (line)
		free(line);
/*	int pad = 2000 / slot;
	int i = 0;
	for (i = 0; i < pad; i++)
	{
		trace[tlen[0]] = 0;
		tlen[0]++;
	} */
	return;
}

void read_strace2(int tmp_len, int tmp_rate, short* trace, int * tlen, int * pcount)
{

	char path[100];
	pcount[0] = 0;
	char *line = NULL;
	size_t len = 0;
	ssize_t read;
	sprintf(path,"traces/strace-%d-%d.trc",tmp_len,tmp_rate);
	FILE * fp = fopen(path,"r");
	tlen[0] = 0;
	while ((read = getline(&line, &len, fp)) != -1)
	{
		trace[tlen[0]] = atoi(line);
		pcount[0] += (double)trace[tlen[0]];
		tlen[0]++;
	}
	fclose(fp);
	printf("\npcount: %d, len: %d, avg: %f\n",pcount[0],tlen[0],((double)pcount[0] / tlen[0]));
	if (line)
		free(line);

	return;
}



// packets from clients to be sent to end server: queue = 0
static int send_cb(struct nfq_q_handle *qh, struct nfgenmsg *nfmsg,
			struct nfq_data *nfa, void *data)
{
//	printf("send_cb called.\n");
	int i;
	struct timeval current;
	unsigned char *buf_data = NULL;
	uint16_t dst_port;
// 	unsigned char buf_data2[PSIZE_MAX];

	if(first_send)
	{
		memcpy(send_buf, buf, rv);
		send_size = rv;
		first_send = 0;
	}
	/* queue: flag passed to call back to tell whether handle_packet is called
	   from inside the program or outside */
	u_int32_t id = 0;
	struct nfqnl_msg_packet_hdr *ph = nfq_get_msg_packet_hdr(nfa);
	if (ph) {
		id = ntohl(ph->packet_id);
	}

	short queue = *((short *)data);

	if(nfq_get_payload(nfa, &buf_data) == -1)
	{
		printf("send_cb nfq_get_payload error.\n");
	}
	struct iphdr *ip = (struct iphdr *) buf_data;
	struct udphdr *udp;

	u_int32_t dst_addr = (unsigned char)(ip->saddr>>24) - 2; // queue index for user: example: 192.168.1.2 is 0


	if(noslot) // only regular cb -> queue = 1
	{
		clients[dst_addr].arrival++;
		clients[dst_addr].heartbeat_arrival++;
		clients[dst_addr].cycle_arrival++;
		clients[dst_addr].heartbeat_processed++;
		clients[dst_addr].processed++;
		if(store)
		{
			store_total_arrival++;
			store_total_processed++;
		}
		heartbeat_total_arrival++;
		heartbeat_total_rate++;
		return nfq_set_verdict(qh, id, NF_ACCEPT, 0, NULL);  // how to treat the pkt after queue
	} else
	{
		if(queue == 0) // packet from clients: to be queued
		{
			if(ip->protocol == IPPROTO_TCP)
			{
				if (clients[dst_addr].active == 1)
				{
					clients[dst_addr].arrival++;
					clients[dst_addr].heartbeat_arrival++;
					clients[dst_addr].cycle_arrival++;
					if (store)
						store_total_arrival++;
					heartbeat_total_arrival++;

					if(clients[dst_addr].q_len > QSIZE - 1)
					{
//						printf("Queue overflow!\n");
						total_loss++;
						clients[dst_addr].loss++;
						heartbeat_total_loss++;
						if (store)
							store_total_loss++;

						return nfq_set_verdict(qh, id, NF_DROP, 0, NULL);  // how to treat the pkt after queue
					}
					else
					{
						gettimeofday(&current, NULL);
					  	memcpy(clients[dst_addr].pkts[clients[dst_addr].tail_idx].pkt, buf, sizeof(buf));
						clients[dst_addr].pkts[clients[dst_addr].tail_idx].pkt_size = rv;
						clients[dst_addr].pkts[clients[dst_addr].tail_idx].pkt_tstamp = current;
						clients[dst_addr].pkts[clients[dst_addr].tail_idx].pkt_port = 0;
						clients[dst_addr].tail_idx = (clients[dst_addr].tail_idx + 1) % QSIZE;
						clients[dst_addr].q_len++;
//						clients[dst_addr].q_len = MIN(clients[dst_addr].q_len + 1,QSIZE);

/*						if (clients[dst_addr].x_value <= 0 && clients[dst_addr].active_traces == 0)
						{
							int new_traces = 1; // 10 / clients[dst_addr].strace_pcount + 1;
							clients[dst_addr].x_value = new_traces;
							clients[dst_addr].active_traces = new_traces;
							for (i = 0; i < new_traces ; i++)
							{
								clients[dst_addr].current_sidx[i] = 0;
								clients[dst_addr].trace[i] = 1;
							}
							clients[dst_addr].err = 0;
							clients[dst_addr].last_err = 0;
						} */

						return nfq_set_verdict(qh, id, NF_STOLEN, 0, NULL);  // how to treat the pkt after queue
					}
				}
				else
				{
					return nfq_set_verdict(qh, id, NF_DROP, 0, NULL);  // how to treat the pkt after queue
				}
			}
			else if (ip->protocol == IPPROTO_UDP)
			{
				udp = (struct udphdr *)(buf_data + sizeof(*ip));
				dst_port = ntohs(udp->dest);
				if (dst_port == 53)
				{
//					printf("DNS from client.\n");
					if (clients[dst_addr].active == 1)
					{
						clients[dst_addr].arrival++;
						clients[dst_addr].heartbeat_arrival++;
						clients[dst_addr].cycle_arrival++;
						if (store)
							store_total_arrival++;

						heartbeat_total_arrival++;
						if(clients[dst_addr].q_len > QSIZE - 1)
						{
//							printf("Queue overflow!\n");
							total_loss++;
							clients[dst_addr].loss++;
							heartbeat_total_loss++;
							if (store)
								store_total_loss++;

							return nfq_set_verdict(qh, id, NF_DROP, 0, NULL);  // how to treat the pkt after queue
						} else
						{
							gettimeofday(&current, NULL);
						  	memcpy(clients[dst_addr].pkts[clients[dst_addr].tail_idx].pkt, buf, sizeof(buf));
							clients[dst_addr].pkts[clients[dst_addr].tail_idx].pkt_size = rv;
							clients[dst_addr].pkts[clients[dst_addr].tail_idx].pkt_tstamp = current;
							clients[dst_addr].pkts[clients[dst_addr].tail_idx].pkt_port = 0;
							clients[dst_addr].tail_idx = (clients[dst_addr].tail_idx + 1) % QSIZE;
							clients[dst_addr].q_len++;
//							clients[dst_addr].q_len = MIN(clients[dst_addr].q_len + 1,QSIZE);
							// jump start
							if (x_value <= 0 && active_traces == 0)
							{
								printf("TRACE JUMP STARTED BY DNS!\n");
								int new_traces = 1; // 10 / clients[dst_addr].strace_pcount + 1;
								x_value = new_traces;
								active_traces = new_traces;
								for (i = 0; i < new_traces ; i++)
								{
									current_sidx[i] = 0;
									trace[i] = 1;
								}
								err = 0;
								last_err = 0;
							}
//							printf("bad DNS\n");
							return nfq_set_verdict(qh, id, NF_STOLEN, 0, NULL);  // how to treat the pkt after queue
						}
					}
					else
						return nfq_set_verdict(qh, id, NF_DROP, 0, NULL);  // how to treat the pkt after queue
				}
				else if (dst_port == 5000)
				{
					if (clients[dst_addr].active == 1)
					{
						clients[dst_addr].arrival++;
						clients[dst_addr].heartbeat_arrival++;
						clients[dst_addr].cycle_arrival++;
						if (store)
							store_total_arrival++;
//						printf("haha called.\n");
						heartbeat_total_arrival++;

						if(clients[dst_addr].q_len > QSIZE - 1)
						{
//							printf("Queue overflow!\n");
							total_loss++;
							heartbeat_total_loss++;
							clients[dst_addr].loss++;
							if (store)
								store_total_loss++;
							return nfq_set_verdict(qh, id, NF_DROP, 0, NULL);  // how to treat the pkt after queue
						} else
						{
							gettimeofday(&current, NULL);
						  	memcpy(clients[dst_addr].pkts[clients[dst_addr].tail_idx].pkt, buf, sizeof(buf));
							clients[dst_addr].pkts[clients[dst_addr].tail_idx].pkt_size = rv;
							clients[dst_addr].pkts[clients[dst_addr].tail_idx].pkt_tstamp = current;
							clients[dst_addr].pkts[clients[dst_addr].tail_idx].pkt_port = 0;
							clients[dst_addr].tail_idx = (clients[dst_addr].tail_idx + 1) % QSIZE;
							clients[dst_addr].q_len++;
//							clients[dst_addr].q_len = MIN(clients[dst_addr].q_len + 1,QSIZE);
							return nfq_set_verdict(qh, id, NF_STOLEN, 0, NULL);  // how to treat the pkt after queue
						}
					}
					else
					{
						return nfq_set_verdict(qh, id, NF_DROP, 0, NULL);  // how to treat the pkt after queue
					}
				}
				else
				{
					printf("unrecognized UDP packet queued up to userspace (%d): DROPPED.\n",dst_port);
					return nfq_set_verdict(qh, id, NF_DROP, 0, NULL);  // how to treat the pkt after queue
				}
			}
			else
			{
				printf("unrecognized packet queued up to userspace: DROPPED.\n");
				return nfq_set_verdict(qh, id, NF_DROP, 0, NULL);  // how to treat the pkt after queue
			}
		}
		else // triggered by timer: read from queue and send it down tunnel
		{
			gettimeofday(&current, NULL);
//			printf("packet to send\n");
			clients[queue - 1].head_idx = (clients[queue - 1].head_idx + 1) % QSIZE;
			clients[queue - 1].q_len--;
//			clients[queue - 1].q_len = MAX(0, clients[queue - 1].q_len - 1);

			return nfq_set_verdict(qh, id, NF_ACCEPT, 0, NULL);
		}
	}
}

// packets from end server to be sent to clients: queue 1
static int recv_cb(struct nfq_q_handle *qh, struct nfgenmsg *nfmsg, struct nfq_data *nfa, void *data)
{
	struct timeval current;
  	u_int32_t id = 0;
  	struct nfqnl_msg_packet_hdr *ph = nfq_get_msg_packet_hdr(nfa);
	if (ph) {
		id = ntohl(ph->packet_id);
	}
	
	unsigned char * packet_data = NULL;
	if(nfq_get_payload(nfa, &packet_data) == -1)
	{
		printf("send_cb nfq_get_payload error.\n");
	}
	struct iphdr *ip = (struct iphdr *) packet_data;
	struct udphdr *udp;
	uint16_t src_port;
	gettimeofday(&current, NULL);
	if(ip->protocol == IPPROTO_TCP)
	{
		if (store)
		{
			fprintf(fp2,"1,%lu.06%lu\n", current.tv_sec % 10000, current.tv_usec);
			fflush(fp2);
			gettimeofday(&current, NULL);
		}
		return nfq_set_verdict(qh, id, NF_ACCEPT, 0, NULL); // how to treat the pkt after queue
	}
	else if (ip->protocol == IPPROTO_UDP)
	{
		udp = (struct udphdr *)(packet_data + sizeof(*ip));
		src_port = ntohs(udp->source);
		if (src_port == 53)
		{
//			printf("DNS from client.\n");
			if (store)
			{
				fprintf(fp2,"1,%lu.06%lu\n", current.tv_sec % 10000, current.tv_usec);
				fflush(fp2);
				gettimeofday(&current, NULL);
			}
			return nfq_set_verdict(qh, id, NF_ACCEPT, 0, NULL);  // how to treat the pkt after queue
		}
		else
		{
			printf("unrecognized UDP packet queued up to userspace (%d): DROPPED.\n",src_port);
			return nfq_set_verdict(qh, id, NF_DROP, 0, NULL);  // how to treat the pkt after queue
		}
	}
	else // real packet: send it to the user
	{
		printf("unrecognized packet queued up to userspace: DROPPED.\n");
		return nfq_set_verdict(qh, id, NF_DROP, 0, NULL); // how to treat the pkt after queue
	}
}


int handle_client_packets(int current_client, int current_rate, int * packets_left)
{
	struct timeval current;
	int i = 0;

	pthread_mutex_lock(&lock);
	packets_left[0] -= MIN(clients[current_client].q_len,current_rate);
	int min_pcount = MIN(clients[current_client].q_len,current_rate);
	clients[current_client].processed += min_pcount; // clients[current_client].last_q_len - clients[current_client].q_len;
	clients[current_client].heartbeat_processed += min_pcount; // clients[current_client].last_q_len - clients[current_client].q_len;
	if (store)
		store_total_processed += min_pcount;
	clients[current_client].last_q_len = clients[current_client].q_len - min_pcount;
	pthread_mutex_unlock(&lock);

	for (i = 0 ; i < min_pcount; i++)
	{
		pthread_mutex_lock(&lock);
		q_flg = current_client + 1; // called from inside: transmit packet (also client no.)
		gettimeofday(&current, NULL);
		struct timeval elapsed;
		timersub(&current,&(clients[current_client].pkts[clients[current_client].head_idx].pkt_tstamp),&elapsed);
		timeradd(&clients[current_client].delay,&elapsed,&clients[current_client].delay);
		memcpy(buf, clients[current_client].pkts[clients[current_client].head_idx].pkt, sizeof(buf));
		rv = clients[current_client].pkts[clients[current_client].head_idx].pkt_size;
		nfq_handle_packet(h, buf, rv); // parameters of cb
//		q_flg = -1;
		pthread_mutex_unlock(&lock);
	}
	return min_pcount;
}

// callback function activated every millisecond
void timer_cb(int sig)
{
	static int current_rate = 1;
	int current_slot = slot;
	int slot_rate = 0;
	int slot_cap = slot * MAX_RATE;

	int i = 0;
	struct timeval current;
	struct timeval before;
	int prob = 0;
	p_param = (int)(p * PRECISION);
	q_param = (int)(q * PRECISION);
	gettimeofday(&before,NULL);
	if (active_clients > 0)
	{
		if(stick % heartbeat == 0 )
		{
			stick = stick % (heartbeat * current_slot * strace_len);
			hb_count++;
			int cc = current_client;
			int ac = active_clients;
			int sum_q_len = 0;
			for (i = 0 ; i < ac ; i++)
			{
//				total_processed += clients[cc].processed;
				sum_q_len += clients[cc].q_len;
				printf("C(%d): {%d,%d,%d}\t",cc + 1,
							clients[cc].heartbeat_arrival,
							clients[cc].heartbeat_processed,
							clients[cc].q_len);
				clients[cc].heartbeat_arrival = 0;
				clients[cc].heartbeat_processed = 0;
				if (clients[cc].ack_received == 0)
				{
					if (clients[cc].missing_acks >= 3)
					{
						if (clients[cc].first_time == 1)
						{
							clients[clients[cc].prev_id].next_id = clients[cc].next_id;
							clients[clients[cc].next_id].prev_id = clients[cc].prev_id;
							if (first_added_id == cc)
								first_added_id = clients[cc].prev_id;
//							last_added_id = clients[cc].next_id;
							last_added_id = clients[cc].prev_id;
							clients[cc].active = 0;
							clients[cc].head_idx = 0;
							clients[cc].tail_idx = 0;
							clients[cc].q_len = 0;
//							t_len -= clients[cc].len;
//							tick = 0;
//							stick = 0;					
//							slack = 0;
							clients[cc].first_time = 0;
							active_clients--;
							printf("user #%u left. active clients: %d, %d>>(%d)>>%d\n",
								cc,
								active_clients,
								clients[clients[cc].next_id].prev_id,
								clients[cc].next_id,
								clients[clients[cc].next_id].next_id);
							if(store)
							{
								store = 0;
								gettimeofday(&current,NULL);
								struct timeval duration;
								timersub(&current,&store_begin_ts,&duration);
								printf("duration: %d, arrival: %d, rate: %d\n",
									store_total_slot,
									store_total_arrival,
									store_total_rate);
								printf("min_active: %d, active_bar: %f, max_active: %d\n",
									store_min_active,
									(double)store_total_active / (double)store_total_slot,
									store_max_active);
								printf("min_x: %d, x_bar: %f, max_x: %d\n",
									store_min_x,
									(double)store_total_x / (double)store_total_slot,
									store_max_x);
								printf("min_y: %f, y_bar: %f, max_y: %f\n",
									store_min_y,
									(double)store_total_y / (double)store_total_slot,
									store_max_y);
								printf("processed: %d, dummy: %d (%f), loss: %d (%f), delay: %f\n",
									store_total_processed,
									store_total_dummy,
									(double)store_total_dummy / (double)(store_total_dummy + store_total_processed),
									store_total_loss,
									(double)store_total_loss / (double)(store_total_arrival),
									store_total_delay / (double)store_total_processed);

								fprintf(fp4,"%d,%.1f,%f,%d,%d,%d,%d,%d, %d,%f,%d, %d,%f,%d, %f,%f,%f, %d,%d,%f, %d,%f,%f\n",
									test_num,
									store_y_value,
									store_alpha,
									strace_pcount,
									strace_len,
									store_total_slot,
									store_total_arrival,
									store_total_rate,
									store_min_active,
									(double)store_total_active / (double)store_total_slot,
									store_max_active,
									store_min_x,
									(double)store_total_x / (double)store_total_slot,
									store_max_x,
									store_min_y,
									(double)store_total_y / (double)store_total_slot,
									store_max_y,
									store_total_processed,
									store_total_dummy,
									(double)store_total_dummy / (double)(store_total_dummy + store_total_processed),
									store_total_loss,
									(double)store_total_loss / (double)(store_total_arrival),
									store_total_delay / (double)store_total_processed);

								fclose(fp);
								fclose(fp2);
								fclose(fp3);
								fclose(fp4);
								fclose(fp5);

								sendto(sockfd, msg_wrap, 5, 0,
									(struct sockaddr *)&servaddr, sizeof(servaddr)); // Initiate UDP channel by sending hello!

							}
						}
					}
					else
					{
						clients[cc].missing_acks++;	
						printf("Client(%d): ACK missed. Strike %d.\n",cc,clients[cc].missing_acks);
						send_sync(cc);
						gettimeofday(&current, NULL);
//						printf("(%d) %lu.%06lu SYNC sent.\n",cc,current.tv_sec,current.tv_usec);
					}
				}
				else
				{
					if (clients[cc].first_time == 0)
						clients[cc].first_time = 1;
					clients[cc].ack_received = 0;
					clients[cc].missing_acks = 0;
					send_sync(cc);
					gettimeofday(&current, NULL);
// 					printf("(%d) %lu.%06lu SYNC sent.\n",cc,current.tv_sec,current.tv_usec);
				}
				if (active_clients == 0)
				{
					last_added_id = -1;
					first_added_id = -1;
					break;
				}
				else
					cc = clients[cc].next_id;
			}
//			printf("loss: %f\n",(double)total_loss / (total_processed + total_loss));

			printf("\nx_value = %d, active_traces: %d, err: %f, trc: %d, arr: %d, srv: %d, q_len: %d, loss: %d\n",
					x_value,
					active_traces,
					err,
					strace_pcount,
					heartbeat_total_arrival,
					heartbeat_total_rate,
					sum_q_len,
					heartbeat_total_loss);


			heartbeat_total_arrival = 0;
			heartbeat_total_rate = 0;
			heartbeat_total_loss = 0;
		}

		// is the stick beginning of current slot?

		if (stick % 100 == 0) // threshold
		{
			fprintf(fp5,"%d\n",active_traces);			
		}

		
		if (stick % (strace_len * current_slot) == 0) // begining of cycle: evaluate y
		{
			int cycle_total_arrival = 0;
			for (i = 0; i < active_clients; i++)
			{
				cycle_total_arrival += clients[current_client].cycle_arrival;
				current_client = clients[current_client].next_id;
			}

			double temp_y = MIN((lambda - mu) * (double)strace_pcount / 2.0,(double)slot * MAX_RATE * (double)strace_len / (double)strace_pcount);
			avg_q_len /= strace_len * current_slot; //reconsider for other slots
			avg_lambda /= strace_len * current_slot; //reconsider for other slots
			y_value = (1.0 - beta) * y_value + beta * temp_y;
			min_q_len = QSIZE;
			avg_q_len = 0;
			avg_lambda = 0.0;
			cycle_rate = 0;

			for (i = 0; i < active_clients; i++)
			{
				clients[current_client].cycle_arrival = 0;
				current_client = clients[current_client].next_id;
			}
		}

		if (stick % current_slot == 0)
		{

			int clients_sum_rate = 0;
			int current_sum_q_len = 0;
			for (i = 0 ; i < active_clients ; i++)
			{
				current_sum_q_len += clients[current_client].q_len;
				clients_sum_rate += rate;
				current_client = clients[current_client].next_id;

			}
			{
				min_q_len = MIN(min_q_len,current_sum_q_len);
				max_arrival = MAX(max_arrival,last_slot_total_arrival);
				avg_arrival = arr_beta * last_slot_total_arrival + (1.0 - arr_beta) * avg_arrival;
				avg_q_len += current_sum_q_len;
				avg_q_len += lambda;
				qmax = MAX(1.0,2.0 * y_value * 2.0 / ((double)strace_pcount * alpha));

				mu = MAX(0,mu + alpha * (y_value * strace_pcount / strace_len - MAX_RATE * slot));

// 				if( stick % current_slot * 100 == 0)
				{
				if (x_value <= 0 && active_traces == 0)
				{
					double thr = ((MIN((double)strace_pcount , (double)current_sum_q_len)) / (double)strace_pcount / 100.0);
					// printf("thr: %f\n",thr);
					thr *= PRECISION;
					prob = rand() % PRECISION;
					// if (prob < thr)
					if (avg_arrival > 5 * arr_beta)
					{
//						printf("NEW TRACE RANDOMLY JUMP STARTED! q_len: %d, prob: %d, thr: %f\n",current_sum_q_len,prob,thr);
						printf("NEW TRACE JUMP STARTED! arrival: %f, thr: %f\n",avg_arrival,(double)strace_pcount / (strace_len * 3));
						int new_traces = 1; // 10 / clients[dst_addr].strace_pcount + 1;
						x_value = new_traces;
						active_traces = new_traces;
						for (i = 0; i < new_traces ; i++)
						{
							current_sidx[i] = 0;
							trace[i] = 1;
						}
						err = 0;
						last_err = 0;
					}
				}
				}


				i = 0;
				int at = active_traces;
				rate = 0;
//						int trace_ended = 0;
				while (at > 0) // sum up rates for this slot
				{
					for (; i < MAX_ACTIVE_TRACES ; i++)
					{
						if (trace[i] == 1)
						{
							rate += super_trace[current_sidx[i]];
							current_sidx[i] = (current_sidx[i] + 1) % strace_len;
							if (current_sidx[i] == 0)
							{
								trace[i] = 0;
								active_traces--;
//										trace_ended = 1;
							}
							at--;
							i++;

							gettimeofday(&current, NULL);
							/* if (store)
							{
								fprintf(fp,"1,%lu.%06lu,%d,%d,NaN,NaN,NaN,NaN,NaN\n",
									current.tv_sec % 10000,
									current.tv_usec,
									current_client,
									active_traces);
								fflush(fp);
							} */
							break;
						}
					}
				}

				// lambda update
				lambda = MAX(0,lambda + alpha * (MAX(last_slot_total_arrival, avg_arrival) - (double)rate));

				if(store)
				{
					fprintf(fp3,"%f,%d,%d,%12f,%d,%12f,%d,%f,%f\n",
						err,
						active_traces,
						x_value,
						y_value,
						rate,
						((double)strace_pcount / strace_len),
						clients[current_client].arrival, // reconsider
						lambda,
						mu);
					fflush(fp3);
				}

				if (stick % (update_cycle * current_slot) == 0) // bergining of cycle: evaluate y
				{
					int temp_x = 0;
	 				err = gamma_value - lambda;
					if (err >= 0)
					{
						if (err >= last_err)
						{
							temp_x = -1; // * (int)(err / (update_cycle * strace_pcount / strace_len) - 0.5);
							last_err = err;
						}
					}
					else
					{
						if (err < last_err)
						{
							temp_x = 1; // * (int)(-err / (update_cycle * strace_pcount / strace_len) - 0.5);
							last_err = err;
						}
					}
					x_value = MAX(0,MIN(MAX_ACTIVE_TRACES,temp_x + x_value));
				}
					
				// activate additional traces
				{
					int req_trace_count = MIN(MAX_ACTIVE_TRACES,x_value) - active_traces;
					i = 0;
					while (req_trace_count > 0)
					{
						for (; i < MAX_ACTIVE_TRACES; i++)
						{
							if (trace[i] == 0)
							{
								trace[i] = 1;
								active_traces++;
								current_sidx[i] = 0;
								gettimeofday(&current, NULL);
								req_trace_count--;
								i++;
								break;
							}
						}
					}
				}
			}
			slot_rate = MIN((int)((double)rate / (1.0 - epsilon)),slot_cap);
			if (store)
				store_total_rate += slot_rate;
			cycle_rate += slot_rate;
			if(!noslot)
				heartbeat_total_rate += slot_rate;
//			report_rate += slot_rate;
			current_rate = MIN((int)((double)rate / (1.0 - epsilon)),slot_cap);

			prob = rand() % PRECISION;
			if (prob < p_param)
			{
				for (i = 0 ; i < active_clients ; i++)
				{
					total_rate += current_rate;
					handle_client_packets(current_client,MIN(current_rate,slot_rate),&slot_rate);
					// prepare for while: the reason we use flag instead of q_len is that q_len might change during this cycle by receiving more packets
					clients[current_client].flag = clients[current_client].q_len;
					// iterate
					current_client = clients[current_client].next_id;
					current_rate = MIN((int)((double)rate / (1.0 - epsilon)),slot_cap);
				}
				if (begin) // dummy rate is allowed
				{
					int temp_len = slot_rate;
					int empty_clients = 0;
//					re-use dummy woth real traffic
//					printf("entered loop\n");
					while ((slot_rate > 0) && (empty_clients != active_clients)) // round robin
					{
						if(clients[current_client].q_len > 0)
						{
							clients[current_client].reused += handle_client_packets(current_client,1, &slot_rate);
							clients[current_client].flag--;
						}
						else // queue not empty: reuse dummy traffic
						{
							if (clients[current_client].flag == 0)
							{
								empty_clients++;
								clients[current_client].flag = 1;
							}
						}
						current_client = clients[current_client].next_id; // iterate
					}
					total_reused += temp_len - slot_rate;
					// no packet queued dummy for rest
					for (i = 0; i < slot_rate; i++)
					{
						gettimeofday(&current, NULL);
						// how does it know who to send dummy to !?
						sendto(sockfd, msg_dummy, 13, 0,
							(struct sockaddr *)&servaddr, sizeof(servaddr));
//						clients[current_client].loss++;
						total_dummy++;
						if (store)
							store_total_dummy++;

//						prob = rand() % PRECISION;
						// printf("prob: %d,\tq_param: %d\n", prob,q_param);
//						if( prob < q_param)
//						{	// send dummy packet
//
					}
					current_client = first_added_id;
				}
			}
//			tick++;

			if (store)
			{
				store_min_active = MIN(active_traces,store_min_active);
				store_total_active += active_traces;
				store_max_active = MAX(active_traces,store_max_active);
				store_min_x = MIN(x_value,store_min_x);
				store_total_x += x_value;
				store_max_x = MAX(x_value,store_max_x);
				store_min_y = MIN(y_value,store_min_y);
				store_total_y += y_value;
				store_max_y = MAX(y_value,store_max_y);
				store_total_slot++;
				int tmp_total_processed = 0;
				gettimeofday(&current, NULL);
				for (i = 0 ; i < active_clients ; i++)
				{
					tmp_total_processed += clients[current_client].processed;
					fprintf(fp,"0,%lu.%06lu,%d,%d,%d,%d,%d,%lu.%06lu,%d\n",
						current.tv_sec % 10000,
						current.tv_usec,
						current_client,
						clients[current_client].arrival,
						clients[current_client].processed,
						clients[current_client].reused,
						clients[current_client].q_len,
						clients[current_client].delay.tv_sec,
						clients[current_client].delay.tv_usec,
						clients[current_client].loss);
					fflush(fp);
					store_total_delay += (double)clients[current_client].delay.tv_sec +
							 (double)clients[current_client].delay.tv_usec / 1000000.0;
					current_client = clients[current_client].next_id; // iterate
				}
				// for multi-user change x_value and y_value to the sum
				fprintf(fp,"0,%lu.%06lu,-1,%d,%d,%d,%d,%d,%d\n",
					current.tv_sec % 10000,
					current.tv_usec,
					MIN(clients_sum_rate,slot_cap), // reconsider
					total_dummy,
					total_reused,
					active_traces,
					x_value,
					tmp_total_processed
					);
				fflush(fp);
			}
			last_slot_total_arrival = 0;
			for (i = 0 ; i < active_clients ; i++)
			{
				clients[current_client].last_slot_arrival = clients[current_client].arrival;
				last_slot_total_arrival += clients[current_client].last_slot_arrival;
				clients[current_client].delay.tv_sec = 0;
				clients[current_client].delay.tv_usec = 0;
				clients[current_client].arrival = 0;
				clients[current_client].loss = 0;
				clients[current_client].processed = 0;
				total_rate = 0;
				clients[current_client].flag = 0;
				clients[current_client].reused = 0;
				// iterate
				current_client = clients[current_client].next_id;
			}
			total_dummy = 0;
			total_loss = 0;
			total_reused = 0;
			gettimeofday(&current, NULL);
			timersub(&current,&before,&current);
		}
	}
	stick++;
}

void* handle_client_socket()
{
	int i;
	struct timeval current;
	u_int32_t dst_addr = -1;
	struct sockaddr_in cliaddr;
	int sockrv = 0;
	int addrlen = sizeof(cliaddr);
	char sockbuf[PSIZE_MAX] __attribute__ ((aligned));
	for(;;)
	{
		if ( ( sockrv = recvfrom(sockfd2, sockbuf, sizeof(sockbuf), 0, (struct sockaddr *)&cliaddr, (socklen_t *)&addrlen ) ) >= 0) {
			dst_addr = (unsigned char)(((struct sockaddr_in)cliaddr).sin_addr.s_addr>>24) - 2; // queue index for user: example: 192.168.1.2 is 0
			if(!(dst_addr == 0 || dst_addr == 1))
			{
				printf("dst_addr: %d\n",dst_addr);
				continue;
			}
			switch (sockbuf[0])
			{
				case PKT_HELLO:
					y_value = 1.0;
					alpha = 1.0; //1.0 / clients[dst_addr].strace_pcount;
					pthread_mutex_lock(&lock);
					if (last_added_id == -1) // first client
					{
//						printf("dst_addr: %d\n",dst_addr);
						clients[dst_addr].prev_id = dst_addr;
						clients[dst_addr].next_id = dst_addr;
						first_added_id = dst_addr;
			 			current_client = first_added_id;
					}
					else
					{
						clients[dst_addr].prev_id = last_added_id;
						clients[dst_addr].next_id = clients[last_added_id].next_id;
						clients[clients[dst_addr].next_id].prev_id = dst_addr;
						clients[last_added_id].next_id = dst_addr;
					}
					clients[dst_addr].first_time = 0;
					clients[dst_addr].id = dst_addr;
					clients[dst_addr].addr = cliaddr;
//					clients[dst_addr].slot = clients[0].slot;
					rate = 0;
					clients[dst_addr].ack_received = 0;
					clients[dst_addr].active = 1;
					clients[dst_addr].head_idx = 0;
					clients[dst_addr].tail_idx = 0;
					clients[dst_addr].q_len = 0;
					clients[dst_addr].last_q_len = 0;
					min_q_len = QSIZE;
					avg_q_len = 0;
					avg_lambda = 0.0;
					max_arrival = 0;
					current_sidx[0] = 0;
					for (i = 0 ; i < MAX_ACTIVE_TRACES ; i++)
						trace[i] = 0;
					active_traces = 0;
					clients[dst_addr].delay.tv_sec = 0;
					clients[dst_addr].delay.tv_usec = 0;
					clients[dst_addr].reused = 0;
					clients[dst_addr].processed = 0;
					clients[dst_addr].heartbeat_processed = 0;
					clients[dst_addr].arrival = 0;
					clients[dst_addr].heartbeat_arrival = 0;
					clients[dst_addr].cycle_arrival = 0;
					clients[dst_addr].loss = 0;
					heartbeat_total_arrival = 0;
					heartbeat_total_rate = 0;
					heartbeat_total_loss = 0;
					clients[dst_addr].last_slot_arrival = 0;
					last_slot_total_arrival = 0;
					clients[dst_addr].flag = 0;
					clients[dst_addr].missing_acks = 0;

 					read_strace(slot, super_trace, &strace_len, &strace_pcount);
					printf("trace loaded, len: %d, pcount: %d\n",strace_len,strace_pcount);
					short pwr = -1;
					memcpy(&pwr, sockbuf + 6,sizeof(pwr));
					int arr = -1;
					memcpy(&arr, sockbuf + 9,sizeof(arr));
					memcpy(&test_num, sockbuf + 14,sizeof(test_num));

//					read_strace2(arr,pwr, super_trace, &strace_len, &strace_pcount);

					alpha = 1.0;
					y_value = (double)arr / ((double)strace_pcount * 1000.0 / (double)strace_len);
					x_value = y_value;
//					printf("test: %d, pow: %d, alpha %f, arrival: %d, y_value: %f\n",test_num, pwr,alpha,arr,y_value);


					// optimization variables
//					x_value = 0.0;

//					update_cycle = 100; // arr (m argument from client)
					sigma = 1.0;
					beta = 0.1;
					arr_beta = 1.0 / 1000.0;
//					gamma_value = 1024; // pwr (n argument from client)
					epsilon = 0.0;
					err = 0.0;
					avg_arrival = 0.0;
					last_err = 0;
					lambda = 0;
					mu = 0;
					if (dynamic)
					{
						update_cycle = pwr;
						gamma_value = arr;
					}
					printf("test: %d, arg(1): %d, arg(2): %d, alpha: %f, cycle: %d, gamma: %f\n",test_num, pwr, arr, alpha, update_cycle, gamma_value);

					// comment these two lines if you want to relay client values of m and n
					arr = (int)update_cycle;
					pwr = (short)gamma_value;


					last_added_id = dst_addr;
					active_clients++;
					printf("user #%u joined. active clients: %d, %d>>(%d)>>%d\n", dst_addr , active_clients, clients[dst_addr].prev_id, dst_addr, clients[dst_addr].next_id);
					send_sync(dst_addr);
					gettimeofday(&current, NULL);
					msg_hello[0] = 'H';
					msg_hello[1] = 'E';
					msg_hello[2] = 'L';
					msg_hello[3] = 'L';
					msg_hello[4] = 'O';
					msg_hello[5] = ' ';
					msg_hello[6] = pwr & 0xFF;
					msg_hello[7] = (pwr >> 8) & 0xFF;
					msg_hello[8] = ' ';
					msg_hello[9] = (arr) & 0xFF;
					msg_hello[10] = ((arr) >> 8) & 0xFF;
					msg_hello[11] = ((arr) >> 16) & 0xFF;
					msg_hello[12] = ((arr) >> 24) & 0xFF;
					msg_hello[13] = ' ';
					msg_hello[14] = test_num & 0xFF;
					msg_hello[15] = (test_num >> 8) & 0xFF;

					sendto(sockfd, msg_hello, 16, 0,
						(struct sockaddr *)&servaddr, sizeof(servaddr)); // Initiate UDP channel by sending hello!


					if(store == 0)
					{
						store_y_value = y_value;
						store_alpha = alpha;
						store_total_rate = 0;
						store_total_arrival = 0;
						store_total_processed = 0;
						store_total_delay = 0.0;
						store_total_dummy = 0;
						store_total_loss = 0;
						store_min_active = MAX_ACTIVE_TRACES + 1;
						store_total_active = 0;
						store_max_active = -1;
						store_min_x = MAX_ACTIVE_TRACES + 1;
						store_total_x = 0;
						store_max_x = -1;
						store_min_y = MAX_ACTIVE_TRACES + 1;
						store_total_y = 0;
						store_max_y = -1;
						store_total_slot = 0;
						gettimeofday(&store_begin_ts,NULL);
						char *pos;
						if ((pos=strchr(filename, '\n')) != NULL)
						    *pos = '\0';
						sprintf(path,"logs/test-%d-up.log",test_num);
						fp = fopen(path,"w");
						sprintf(path,"logs/test-%d-down.log",test_num);
						fp2 = fopen(path,"w");
						sprintf(path,"logs/test-%d-err.log",test_num);
						fp3 = fopen(path,"w");
						sprintf(path,"logs/test-results.log");  
						fp4 = fopen(path,"a");
						sprintf(path,"logs/test-%d-ac.log",test_num);
						fp5 = fopen(path,"w");

						store = 1;						
					}

					pthread_mutex_unlock(&lock);
// 					printf("(%d) %lu.%06lu SYNC sent.\n",dst_addr, current.tv_sec,current.tv_usec);
					break;
				case PKT_ACK:
					gettimeofday(&current, NULL);
					pthread_mutex_lock(&lock);
					clients[dst_addr].ack_received = 1;
					pthread_mutex_unlock(&lock);
					
// 					printf("(%d) %lu.%06lu ACK received\n",dst_addr, current.tv_sec,current.tv_usec);
					break;
				default:
					printf("unknown UDP packet: dropped.\n");
					break;
			}
			continue;
		}
		if (sockrv < 0 && errno == ENOBUFS) {
			printf("losing packets!\n");
			continue;
		}
		perror("recv failed");
		break;
	}
	return NULL;
}

void* handle_server_socket()
{
	int sockrv = 0;
	int addrlen = sizeof(servaddr);
	struct timeval current;
	char sockbuf[PSIZE_MAX] __attribute__ ((aligned));
	for(;;)
	{
		if ( ( sockrv = recvfrom(sockfd, sockbuf, sizeof(sockbuf), 0, (struct sockaddr *)&servaddr, (socklen_t *)&addrlen ) ) >= 0) {
			switch (sockbuf[0])
			{
				case PKT_SYNC:
					sendto(sockfd, msg_ack, 4, 0,
						(struct sockaddr *)&servaddr, sizeof(servaddr)); // Initiate UDP channel by sending hello!
					break;
				case PKT_DUMMY:
					if (store)
					{
						gettimeofday(&current, NULL);
						fprintf(fp2,"0,%lu.06%lu\n", current.tv_sec % 10000, current.tv_usec);
						fflush(fp2);
					}
//					printf("dummy received and ignored.\n");
					break;
				default:
					printf("unknown UDP packet: dropped.\n");
					break;
			}
			continue;
		}
		if (sockrv < 0 && errno == ENOBUFS) {
			printf("losing packets!\n");
			continue;
		}
		perror("recv failed");
		break;
	}
	return NULL;
}
// thread for reading packets from udp tunnel
void* handle_packet()
{
//	int packet_num = 0;
	char tmpbuf[PSIZE_MAX] __attribute__ ((aligned));
	int tmprv = -1;
	for(;;)
	{
//		printf("packet: %d\n",packet_num);
		if ((tmprv = recv(fd, tmpbuf, sizeof(tmpbuf), 0)) >= 0) {
			pthread_mutex_lock(&lock);
			memcpy(buf, tmpbuf, sizeof(tmpbuf));
			rv = tmprv;
//			heartbeat_total_arrival++;
//			printf("q_flg: %d\n",q_flg);
			q_flg = 0; // called from outside: queue the packet
			nfq_handle_packet(h, buf, rv); // parameters of cb
//			packet_num++;
//			q_flg = -1; // called from outside: queue the packet
			pthread_mutex_unlock(&lock);
			continue;
		}
		if (rv < 0 && errno == ENOBUFS) {
			printf("losing packets!\n");
			continue;
		}
		perror("recv failed");
		break;
	}
	return NULL;
}

void *handle_clock()
{
	sigset_t waitset;
	struct itimerval timer;
	int sig;
	int result = 0;

	sigemptyset(&waitset);
	sigaddset(&waitset, SIGALRM);
	// sigprocmask(SIG_BLOCK, &waitset, NULL);

	timer.it_value.tv_sec = 0;//waits for 5 seconds before sending timer signal
	timer.it_value.tv_usec = 1000;
	timer.it_interval.tv_sec = 0;//sends timer signal every 5 seconds
	timer.it_interval.tv_usec = 1000;

	setitimer(ITIMER_REAL, &timer, NULL);

	while(1)
	{
		result = sigwait(&waitset, &sig);
		if(result == 0) // success
			timer_cb(sig);
		else
		{
			printf("sigwait() returned error number %d\n", errno);
			perror("sigwait() function failed\n");
		}
	}

}



int main(int argc, char** argv)
{
	int i = 0;
	int rset = 0;
	slot = 1;
	rate = 2147483647;

	for (i = 0 ; i < CLIENTS; i++)
	{
		clients[i].active = 0;
	}
	printf("size: %lu\n",sizeof(clients[0]));

	for (i = 1 ; i < argc; i++)
	{
		if(!strcmp(argv[i],"-s"))
			slot = atoi(argv[++i]);
		else if(!strcmp(argv[i],"-p"))
		{
			p = atof(argv[++i]);
		}
		else if(!strcmp(argv[i],"-q"))
		{
			q = atof(argv[++i]);
			begin = 1;
		}
		else if(!strcmp(argv[i],"-r"))
		{
			rate = atoi(argv[++i]);
			rset = 1;
		}
		else if(!strcmp(argv[i],"-uc"))
		{
			update_cycle = atoi(argv[++i]);
		}
		else if(!strcmp(argv[i],"-g"))
		{
			gamma_value = atof(argv[++i]);
		}
		else if(!strcmp(argv[i],"-debug"))
			debug = 1;
		else if(!strcmp(argv[i],"-noslot"))
			noslot = 1;
		else if(!strcmp(argv[i],"-dynamic"))
			dynamic = 1;
		else if(!strcmp(argv[i],"-store"))
		{
			sprintf(filename,"%s",argv[++i]);
			store = 1;
		}
		else
		{
			printf("Usage: udpserver [OPTION]\n");
//			printf("  -s,\t\t slot size in milliseconds - default: 1\n");
			printf("  -uc,\t\t update cycle in milliseconds - default: 100\n");
			printf("  -g,\t\t gamma parameter - default: 1024\n");
//			printf("  -q,\t\t probability of sending dummy packets - default: 0\n");
//			printf("  -r,\t\t packets per slot - default: max\n");
//			printf("  -t,\t\t total cycle length as multiplier of small cycle - default: small cycle\n");
			printf("  -noslot,\t no time slotting\n");
//			printf("  -debug,\t run in debug mode\n");
			printf("  -store,\t store timestamps\n");
			printf("  -dynamic,\t set parameters with client's arguments\n");
			return 1;
		}
	}

	if(noslot)
		begin = 0;
	else
		begin = 1;
	if(!rset)
	{
		if (begin)
		{
			printf("high cycle rate is set to 10 packets by default\n");
			rate = 1;
		}
		else
			rate = slot * 1000 / 50;
	}
	int bad_input = 0;
	if(slot * 1000 / 50 < rate)
	{
		printf("Input Error: high cycle rate higher than capacity.\n");
		printf("high cycle rate: %d, max rate: %d\n",rate,(slot * 1000 / 50));
		bad_input = 1;
	}
	if(bad_input)
	{
		printf("For instructions type: udpclient -?\n");
		return 1;
	}
	printf("slot = %d(ms) , rate = %d , q = %f , begin = %d\n", slot, rate,q,begin);


	char *pos;
	if ((pos=strchr(filename, '\n')) != NULL)
	    *pos = '\0';
	sprintf(path,"logs/test-results.log");  
	fp4 = fopen(path,"w");
	fclose(fp4);

	if(store)
	{
		store_total_rate = 0;
		store_total_arrival = 0;
		store_total_processed = 0;
		store_total_delay = 0.0;
		store_total_dummy = 0;
		store_total_loss = 0;
		store_min_active = MAX_ACTIVE_TRACES + 1;
		store_total_active = 0;
		store_max_active = -1;
		store_min_x = MAX_ACTIVE_TRACES + 1;
		store_total_x = 0;
		store_max_x = -1;
		store_min_y = MAX_ACTIVE_TRACES + 1;
		store_total_y = 0;
		store_max_y = -1;
		store_total_slot = 0;
		gettimeofday(&store_begin_ts,NULL);
		sprintf(path,"logs/%s-up.log",filename);
		fp = fopen(path,"w");
		sprintf(path,"logs/%s-down.log",filename);
		fp2 = fopen(path,"w");
		sprintf(path,"logs/%s-err.log",filename);  
		fp3 = fopen(path,"w");
	}

	// clients socket
	sockfd2 = socket(AF_INET, SOCK_DGRAM, 0);
	bzero(&servaddr2, sizeof(servaddr2));
	servaddr2.sin_family = AF_INET;
	// change address if server address is set to something different
	servaddr2.sin_addr.s_addr = htonl(INADDR_ANY);
	servaddr2.sin_port = htons(47385);
	bind(sockfd2, (struct sockaddr *)&servaddr2, sizeof(servaddr2));
	
	// server socket
	sockfd = socket(AF_INET,SOCK_DGRAM,0);
	bzero(&servaddr, sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	// change address if server address is set to something different
	servaddr.sin_addr.s_addr = inet_addr("192.200.1.1");
	servaddr.sin_port = htons(47380);
	
// NET_FILTER Initialization
	printf("opening library handle\n");
	h = nfq_open();
	if (!h) {
		fprintf(stderr, "error during nfq_open()\n");
		exit(1);
	}

	printf("unbinding existing nf_queue handler for AF_INET (if any)\n");
	if (nfq_unbind_pf(h, AF_INET) < 0) {
		fprintf(stderr, "error during nfq_unbind_pf()\n");
		exit(1);
	}

	printf("binding nfnetlink_queue as nf_queue handler for AF_INET\n");
	if (nfq_bind_pf(h, AF_INET) < 0) {
		fprintf(stderr, "error during nfq_bind_pf()\n");
		exit(1);
	}

	printf("binding 'send to client' socket to queue '0'\n");
	sqh = nfq_create_queue(h,  0, &send_cb, &q_flg); // cb will act as callback
	if (!sqh) {
		fprintf(stderr, "error during nfq_create_queue()\n");
		exit(1);
	}
	printf("binding 'receive from client' socket to queue '1'\n");
	rqh = nfq_create_queue(h,  1, &recv_cb, NULL); // cb will act as callback
	if (!rqh) {
		fprintf(stderr, "error during nfq_create_queue()\n");
		exit(1);
	}

	printf("setting copy_packet mode\n");
	if ((nfq_set_mode(sqh, NFQNL_COPY_PACKET, 0xffff) < 0) || (nfq_set_mode(rqh, NFQNL_COPY_PACKET, 0xffff) < 0)) {
		fprintf(stderr, "can't set packet_copy mode\n");
		exit(1);
	}

	printf("setting max_q_length\n");
	if ((nfq_set_queue_maxlen(sqh, FILTER_QSIZE) < 0) || (nfq_set_queue_maxlen(rqh, FILTER_QSIZE) < 0)) {
		fprintf(stderr, "can't set queue_maxlen\n");
		exit(1);
	}

	printf("setting buffer_size\n");
	if ((nfnl_rcvbufsiz(nfq_nfnlh(h), 1500 * FILTER_QSIZE) < 0)) {
		fprintf(stderr, "can't set queue_maxbuffer\n");
		exit(1);
	}

	fd = nfq_fd(h);
	short pwr = 1;
	int arr = 1;
	test_num = 0;
// negotiate initial gamma and cycle length with node B
	msg_hello[0] = 'H';
	msg_hello[1] = 'E';
	msg_hello[2] = 'L';
	msg_hello[3] = 'L';
	msg_hello[4] = 'O';
	msg_hello[5] = ' ';
	msg_hello[6] = pwr & 0xFF;
	msg_hello[7] = (pwr >> 8) & 0xFF;
	msg_hello[8] = ' ';
	msg_hello[9] = arr & 0xFF;
	msg_hello[10] = (arr >> 8) & 0xFF;
	msg_hello[11] = (arr >> 16) & 0xFF;
	msg_hello[12] = (arr >> 24) & 0xFF;
	msg_hello[13] = ' ';
	msg_hello[14] = test_num & 0xFF;
	msg_hello[15] = (test_num >> 8) & 0xFF;
	sendto(sockfd, msg_hello, 16, 0,
		(struct sockaddr *)&servaddr, sizeof(servaddr)); // Initiate UDP channel by sending hello!
//	printf("said hello\n");
	pthread_t send_thread;
	pthread_t client_socket_thread;
	pthread_t server_socket_thread;
	pthread_t clock_thread;

	// struct sigaction sa;
	sigset_t newset;
	sigemptyset (&newset);
	sigaddset(&newset, SIGALRM);
	sigprocmask( SIG_BLOCK, &newset, NULL);

	pthread_mutex_init(&lock, NULL);
	
	// pthread_create(&heartbeat_thread, NULL, handle_heartbeat, NULL);
	pthread_create(&send_thread, NULL, handle_packet, NULL);	
	pthread_create(&client_socket_thread, NULL, handle_client_socket, NULL);
	pthread_create(&server_socket_thread, NULL, handle_server_socket, NULL);
	pthread_create(&clock_thread, NULL, handle_clock, NULL);
	
	// pthread_join(heartbeat_thread, NULL);
	pthread_join(send_thread, NULL);
	pthread_join(client_socket_thread, NULL);
	pthread_join(server_socket_thread, NULL);
	pthread_join(clock_thread, NULL);
	
	pthread_mutex_destroy(&lock);

	printf("unbinding from queue 0\n");
	nfq_destroy_queue(sqh);
	printf("unbinding from queue 1\n");
	nfq_destroy_queue(rqh);
#ifdef INSANE
	/* normally, applications SHOULD NOT issue this command, since
	 * it detaches other programs/sockets from AF_INET, too ! */
	printf("unbinding from AF_INET\n");
	nfq_unbind_pf(h, AF_INET);
#endif
	printf("closing library handle\n");
	nfq_close(h);
	exit(0);
}

